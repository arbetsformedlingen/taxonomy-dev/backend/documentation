# Installing Torch with GPU support on Ubuntu

These are sparse notes on how to get Torch to work with the GPU (presumably Nvidia) on Ubuntu. Right now, these instructions are mainly for *Ubuntu 20.04* but I suppose it will not be very different for other versions of Ubuntu:

1. *Optional step:* If you already have Nvidia drivers installed and have already experienced problems, you may want to run `nvidia-smi`. In case you get an error, read more [here](https://forums.developer.nvidia.com/t/nvidia-smi-has-failed-because-it-couldnt-communicate-with-the-nvidia-driver-make-sure-that-the-latest-nvidia-driver-is-installed-and-running/197141).

2. Make sure you have *recent* drivers installed. You may want to uninstall old drivers (e.g. `sudo apt remove nvidia-driver-465`) and install new drivers (e.g. `sudo apt install nvidia-driver-515`, there may be a newer version than this).

3. Here is a good guide for installing Torch on Ubuntu: [How to Install PyTorch on Ubuntu 20.04](https://varhowto.com/install-pytorch-ubuntu-20-04/). It boils down to something like
   - Install Pip: `sudo apt install python3-pip`
   - Install Cuda: `sudo apt install nvidia-cuda-toolkit`
   - Check Cuda version: `nvcc -V`. You will get something like this in return: `nvcc: NVIDIA (R) Cuda compiler driver
Copyright (c) 2005-2019 NVIDIA Corporation
Built on Sun_Jul_28_19:07:16_PDT_2019
Cuda compilation tools, release 10.1, V10.1.243`. According to this message, the version is **10.1**.
   - Based on [**this page**](https://pytorch.org/get-started/previous-versions/) and the Cuda version that you observed, install the right Torch libraries for that version: `pip3 install torch==1.7.1+cu101 torchvision==0.8.2+cu101 torchaudio==0.7.2 -f https://download.pytorch.org/whl/torch_stable.html`. **The version does not have to match perfectly:** *for instance, if `nvcc -V` reported 11.5, then Cuda for 11.3 will probably do.*
   - You may want to add `~/.local/bin` to the path by adding `export PATH=/home/ubuntu/bin:/home/ubuntu/.local/bin:$PATH` to your `~/.bashrc` file.

4. You will probably have to restart your computer.
5. Check that Torch is working in Python:

```
>>> import torch
>>> print(torch.rand(5, 3))
tensor([[0.1019, 0.8169, 0.2027],
        [0.9660, 0.7780, 0.4857],
        [0.3270, 0.6250, 0.8385],
        [0.2675, 0.4834, 0.0209],
        [0.4230, 0.2058, 0.5049]])
>>> import torch
>>> torch.cuda.is_available()
True
```
