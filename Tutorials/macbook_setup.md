# Macbook setup

This document explains how to configure a Macbook for development of the Taxonomy API. We will assume that your username on Macbook is `jtuser` and your home directory is located at `/Users/jtuser` but you need to replace it by *your username*.

You may want to skip some of the steps below depending on personal preferences.

## Mac-specific documentation

You find documentation specific to Mac at <https://serviceportal.arbetsformedlingen.se/sp> and click on the **Kunskapsdatabas**. In the search box, type *"mac"*.

The following documents are of particular interest

* [MAC - När du startar upp din mac första gången..][1]
* [MAC - Kom igång med outlook (aktivera office365)][2]
* [MAC - Så startar du Egenrapporteringen][3]

## Basic configuration

Once you have received your Mac, just follow the instructions in [MAC - När du startar upp din mac första gången..][1].

## Install e-mail client: Outlook

Follow the instructions in [MAC - Kom igång med outlook (aktivera office365)][2]

## Using Egenrapportering

See [MAC - Så startar du Egenrapporteringen][3]. See specifically the instruction about *holding down the Control key*:

> Väljer man att öppna den nedladdade filen via Dock så måste man samtidigt hålla in control-tangenten när man klickar på filen

If Egenrapportering still fails to open, try to open it through *System settings*:

![Egenrapportering](egenrapportering.png)

## Install iTerm2

The **iTerm2** is a terminal emulator that supports split windows, unlike the default emulator Terminal.

Download the zip from <https://iterm2.com/#/section/downloads>, unpack it and run the `iTerm2` executable. It will let you add it to the Applications folder.

## Install Xcode and Homebrew

Go to the terminal and run `xcode-select --install`. Look for a new window (*it may be minimized*). Run the installer.

*Install Homebrew:* Go to https://github.com/Homebrew/brew/releases/ and download the *latest Pkg installer*. Run it. **Pay attention to the message at the end of the installation**. It will ask you to run something like

```
eval "$(/opt/homebrew/bin/brew shellenv)"
```

## Install Git

Run the following commands:

```
brew update
brew install git-gui
brew install git bash-completion
```

To get bash completion working for Git, you may have to also add the line
```
[[ -r "/opt/homebrew/etc/profile.d/bash_completion.sh" ]] && . "/opt/homebrew/etc/profile.d/bash_completion.sh"
```
to your `/Users/jtuser/.zshrc` file. And maybe, according to <https://stackoverflow.com/a/58517668>, run the following in the terminal:

```
echo 'autoload -Uz compinit && compinit' >> ~/.zshrc && . ~/.zshrc
```

## Install Babashka

Just run `brew install borkdude/brew/babashka`.

## Setup an SSH key (for Git repos)

Run `ssh-keygen -t ed25519 -C "jtuser@gmail.com"` replacing `jtuser@gmail.com` by your e-mail address.

## Install JDK

Download the **Pkg installer** from https://adoptium.net/installation/macos/ and run it.

## Install Clojure

Run `brew install clojure/tools/clojure` in the terminal.

## Install Mattermost

Go to <https://mattermost.com/apps/>, create an AppleID and install it from App Store.

## Install Graphviz

Graphviz is needed for the taxonomy:

Just run `brew install graphviz`.

## Tools for Python programming

See <https://www.freecodecamp.org/news/python-version-on-mac-update/>: Just run `brew install pyenv`.

It will display `Installed Python-3.12.5 to /Users/osdjn/.pyenv/versions/3.12.5`.

Update the `/Users/jtuser/.zshrc` setting of the `PATH` to include `/usr/local/bin`: `export PATH=/usr/local/bin:/Users/osdjn/local/bin:${PATH}`

Install **pipx** according to <https://pipx.pypa.io/stable/installation/>:

```
brew install pipx
pipx ensurepath
```

Install **Poetry** according to <https://python-poetry.org/docs/#installation>:

```
pipx install poetry
```

Also install torch if you like: `pipx install torch`

## Install Podman

First call `brew install podman`.

It will print something like the following:
```
==> Caveats
In order to run containers locally, podman depends on a Linux kernel.
One can be started manually using `podman machine` from this package.
To start a podman VM automatically at login, also install the cask
"podman-desktop".

zsh completions have been installed to:
  /opt/homebrew/share/zsh/site-functions
==> Summary
🍺  /opt/homebrew/Cellar/podman/5.3.1_1: 201 files, 77.0MB
==> Running `brew cleanup podman`...
Disable this behaviour by setting HOMEBREW_NO_INSTALL_CLEANUP.
Hide these hints with HOMEBREW_NO_ENV_HINTS (see `man brew`).
osdjn@98-33305 djs-tax2skos %
```

Then run `podman machine init` followed by `podman machine start`.

## Tools for C++ programming

Run in the terminal:
```
brew install llvm
brew install cmake
```

## Install Emacs

Download Emacs from <https://emacsformacos.com/> and install it.

Create a file `/Users/jtuser/.zshrc` (replacing `osdjn` by your username) and add the line

```
export PATH=/Users/jtuser/local/bin:${PATH}
```
to that file. Create a file `/Users/jtuser/local/bin/emacs` with the following contents:

```
#!/bin/sh
/Applications/Emacs.app/Contents/MacOS/Emacs "$@"
```

and run `chmod u+rwx /Users/jtuser/local/bin/emacs` to make it executable.

See more info about Emacs on Mac here:  <https://www.emacswiki.org/emacs/EmacsForMacOS>.

Also consider copying the `~/.emacs.d` directory from your previous setup. If you have a git repo with your elisp scripts and want a custom configuration for your Macbook, you can define a `LOCATION` variable in your `/Users/jtuser/.zshrc` file: `export LOCATION=jobtech` and then have the following elisp code somwhere in your `~/.emacs.d/init.el` file:

```
(defun configure-default ()
  (setq-default tab-width 2)
  (setq js-indent-level 2)
  (setq-default indent-tabs-mode nil)
  (set-face-attribute 'default nil :height 120))

(x-focus-frame nil)

(defun macbook-meta-and-altgr ()
  ;; https://stackoverflow.com/a/13123970
  (setq mac-option-key-is-meta t)
  (setq mac-right-option-modifier nil))

(defun configure-macbook ()
  (macbook-meta-and-altgr)
  (set-face-attribute 'default nil :height 160)
  (global-set-key (kbd "¶") (lambda () (interactive) (insert "|")))
  (global-set-key (kbd "|") (lambda () (interactive) (insert "{")))
  (global-set-key (kbd "≈") (lambda () (interactive) (insert "}")))
  (global-set-key (kbd "§") (lambda () (interactive) (insert "<")))
  (global-set-key (kbd "°") (lambda () (interactive) (insert ">")))
  (global-set-key (kbd "±") (lambda () (interactive) (insert "\\")))
  (global-set-key (kbd "´") 'cider-repl-set-ns))

(defun configure-jobtech ()
  (configure-macbook))

(let* ((location (getenv "LOCATION")))
  (cond
   ((string= "jobtech" location) (configure-jobtech))
   (t (configure-default))))
```
The above code remaps some keys for *Swedish* keyboard so that the Mac keyboard is mapped in a similar way to a PC keyboard inside Emacs. It also changes the font size a bit and. Modify the code according to your preferences.


## Install LaTeX

Although there are brew-packages such as `mactex` and `basictex`, they require root-access. A better approach may be to perform a [custom installation](https://pwsmith.github.io/2020/06/16/custom-install-texlive-on-mac/):

Start by downloading `install-tl-unx.tar.gz` from <https://www.tug.org/texlive/acquire-netinstall.html>, unpack it and launch the installer:

```
osdjn@98-33305 % tar -xvf "install-tl-unx.tar.gz"
osdjn@98-33305 % cd install-tl-20241209
osdjn@98-33305 % perl install-tl -no-gui
```

Then configure a custom installation path and launch the installation:
```
Enter command: D
Enter command: 1
New value for TEXDIR [/usr/local/texlive/2024]: /Users/osdjn/local/bin/texlive/2024
Enter command: R
Enter command: I
```

At the end of the installation, you will see something like
```
Add /Users/osdjn/local/bin/texlive/2024/texmf-dist/doc/man to MANPATH.
Add /Users/osdjn/local/bin/texlive/2024/texmf-dist/doc/info to INFOPATH.
Most importantly, add /Users/osdjn/local/bin/texlive/2024/bin/universal-darwin
to your PATH for current and future sessions.

Logfile: /Users/osdjn/local/bin/texlive/2024/install-tl.log
```

Complete the installation by updating the `PATH` variable in `~/.zshrc`, e.g.
```
export PATH="$PATH:/Users/osdjn/local/bin/texlive/2024/bin/universal-darwin"
```

## Ruby

The Ruby installation that comes with MacOS cannot be easily updated without sudo rights. Therefore, it may be better to install it with Brew, see [this Stack Overflow answer](https://stackoverflow.com/a/54873916):

```
brew install chruby ruby-install
ruby-install ruby
```

Pay attention to the output:
```
osdjn@98-33305 ~ % ruby-install ruby
>>> Updating ruby versions ...
>>> Installing ruby 3.4.1 into /Users/osdjn/.rubies/ruby-3.4.1 ...
>>> Installing dependencies for ruby 3.4.1 ...
==> Downloading https://formulae.brew.sh/api/formula.jws.json
==> Downloading https://formulae.brew.sh/api/cask.jws.json
...
```

You will have to add the path to the `bin` directory and the path to the gems to your `PATH` variable by editing `~/.zshrc`. It may look like this:

```
export PATH="/Users/osdjn/.rubies/ruby-3.4.1/lib/ruby/gems/3.4.0:/Users/osdjn/.rubies/ruby-3.4.1/bin:$PATH"
```

and then run
```
source ~/.zshrc
```

Check that you properly pointed out the correct binary with `which ruby`.

Also take a look at
```
gem environment
```

which will output something like
```
  - GEM PATHS:
     - /Users/osdjn/.rubies/ruby-3.4.1/lib/ruby/gems/3.4.0
     - /Users/osdjn/.gem/ruby/3.4.0
```

## Install Asciidoctor

First install Ruby, see above. It is needed for Asciidoctor. Also make sure Xcode is installed, see above.

Then install Asciidoctor:
```
gem install asciidoctor
```

Then check that it has been correctly installed with

```
asciidoctor -v
```


It should print something like the following:
```
Asciidoctor 2.0.23 [https://asciidoctor.org]
Runtime Environment (ruby 3.4.1 (2024-12-25 revision 48d4efcb85) +PRISM [arm64-darwin24]) (lc:UTF-8 fs:UTF-8 in:UTF-8 ex:UTF-8)
```

When using Asciidoctor from a project that needs it, you may have to run
```
bundle update --bundler
```

Also, you may want to run
```
gem install asciidoctor-pdf
```



[1]: https://serviceportal.arbetsformedlingen.se/sp?sys_kb_id=3806345d30e3e1503a06f116b9653022&id=af_sp2_kb_article_view&sysparm_rank=1&sysparm_tsqueryId=28eb150a442c16903a069ef5fe6bd197
[2]: https://serviceportal.arbetsformedlingen.se/sp?sys_kb_id=fe6294a4181c75503a064943fd76b8dc&id=af_sp2_kb_article_view&sysparm_rank=1&sysparm_tsqueryId=c73b9946442c16903a069ef5fe6bd19b
[3]: https://serviceportal.arbetsformedlingen.se/sp?sys_kb_id=58bbe97d60cb5d143a064615e6825a62&id=af_sp2_kb_article_view&sysparm_rank=1&sysparm_tsqueryId=d66b1186442c16903a069ef5fe6bd169
