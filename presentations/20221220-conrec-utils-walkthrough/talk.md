# conrec-utils walkthrough

Hello and welcome to this walktrough of the conrec-utils Python library.
This is going to be a very practical walkthrough in order to teach you how
you can take advantage of this library to be more productive when
developing concept recognition algorithms. The idea is that conrec-utils
really takes care of all the routine tasks such as working with datasets,
computing evaluation metrics and rendering the results, so that *you* can
focus on implementing good algorithms.

## What is conrec-utils?

* OK, so as I previous mentioned, conrec-utils is a Python library to help
when implementing concept recognition algorithms. However, it can also be
used as a command-line application when doing concept recognition stuff and
we will see how.
* It helps with working with datasets of annotated documents used for concept
recognition and it takes care of things such as loading them and provides also
predefined splits into **train**, **validation** and **test** so that you don't
have to do that.
* And, as a I mentioned, it can be used to evaluate concept recognition algorithms
and render or report results to make it easy to understand how an algorithm performs.

## This walkthrough

OK, so what will we look at today? We will look at conrec-utils step-by-step:

* First, we will start a new Python project and link it to conrec-utils.
* Then, we will take a look at how we can access the taxonomy from conrec utils.
* Then, we will show how we can access annotated document datasets.
* Then we will implement a stupid algorithm just for the sake of seeing how conrec-utils performs.
But the idea is that you should use conrec-utils to implement a *smart* algorithm.
* Then we will run the algorithm agains a dataset to compute metrics.
* We will also render a report to understand how the algorithm performs.
* Then we will make a web service.
* And finally, we will look at how we can compare it against other algorithms.
