# Manus

## Begreppsigenkänning

Nu tänkte jag prata om ett område som vi är intresserade av och som har stark anknytning till taxonomin som Henrik presenterade.

Jag ska prata om begreppsigenkänning.

Kortfattat innebär det att man letar upp taxonomibegrepp i skriven text. Texten kan t.ex. vara jobbannonser, utbildningsbeskrivningar eller andra dokument med anknytning till arbetsmarknaden.

Tanken med begreppsigenkänning är att maskinellt förstå innehållet i en skriven text och kunna representera det på ett sätt som gör informationen lätt att bearbeta.

Detta kan möjliggöra bättre indexering av information, sökning och matchning av t.ex. arbetstagare och arbetsgivare.

Så nu ska vi titta lite närmre, mer konkret på det där med begreppsigenkänning.

## Begreppsigenkänning, manuell

Så vad menar vi då med begreppsigenkänning?

Här visar jag en bild för att bättre förklara vad vi menar. Vi har ett dokument, och i det här dokumentet har vi färgat olika ord. De orden som vi har färgat är begrepp i taxonomin.

Tag till exempel ordet CNC-operatör som vi har markerat här. Det är knutet till ett begrepp ur taxonomin av typen "occupation-name", d.v.s. "yrkesbenämning" och det begreppet råkar ha just etiketten "CNC-operatör/FMS-operatör". Det här är bara ett exempel av alla de begrepp som har märkts upp i texten.

Tanken med begreppsigenkänning är alltså att hitta begrepp som omnämns i en text som ett första steg att maskinellt förstå vad en text handlar om, uttryckt med hjälp av begrepp ur taxonomin. Detta fångar förstås inte hela textens innebörd. I vilket sammanhang ett begrepp omnämns spelar också roll för textens innebörd. Men detta är något som vi tänker oss tackla i ett senare skede. Just nu koncentrerar vi oss på delproblemet att på ett robust sätt hitta begreppen där de förekommer i texten.

Det här arbetet med att märka upp begreppen har inte gjorts av en dator utan manuellt med hjälp av ett webbaserat verktyg, jobtech annotation editor, och det är just en skärmbild av det verktyget som du ser här. Som du kanske minns hade vi några sommarjobbare här och det var just det här de arbetade med: att märka upp dokument med begrepp. De dokument som de märkte upp var främst (1) jobbannonser och (2) utbildningsbeskrivningar från Myndigheten för Yrkeshögskolan (MYH).

De uppmärkta dokumentade sparas som json-kodade filer i ett allmänt tillgängligt gitrepo.

Och då frågar man sig, varför är det här användbart? Det tänkte jag beröra nu.

(visa eventuellt verktyget)

## Annoteringsdata 16:13

Processen med att annotera dokument kan sammanfattas på följande vis. Vi utgår från en text, som exempelvis en jobbannons eller utbildningsbeskrivning. Sedan får en person manuellt gå igenom texten och markera ord i texten och koppla dessa markerade ord mot begrepp i taxonomin 

Det som är indata till annoteringsprocessen är alltså ett dokument. Och resultatet, utdatan, av den här processen, det är vilka begrepp som förekommer i texten och var begreppen förekommer.

Syftet med att vi gör det här är att vi vill bygga en algoritm som kan ersätta manuellt arbete. Men det är också en chans att göra nya upptäckter och lära oss mer om begrepp och dokument inom arbetsmarknadsområdet.

Mer konkret så är kanske den viktigaste användningen att kunna mäta hur bra en begreppsigenkänningsalgoritm fungerar, så att när vi bygger algoritm kan vi se om algoritmen innebär en förbättring i jämförelse med algoritmer som vi hade tidigare.

Ett annat användningsområde är maskininlärning, d.v.s. att man lär ett datorprogram att beräkna någonting genom många exempel av indata och utdata. Dessa exempel består i det här fallet av dokument och motsvarande annoteringar och på så vis lär sig programmet att annotera ett nytt dokument som det inte har sett tidigare.

Slutligen kan vi utforska dessa data för att det är intressant och lära oss mer om arbetsmarknadsdata.

Nu tänkte jag att vi skulle titta lite kort på hur annoteringsdata ser ut.

## Representation av annoteringsdata 16:15

Här har vi en representation i json-format av det dokument som vi tittade på tidigare. Den består i första hand av dokumentets text och förekomster av begrepp i texten, så kallade annoteringar. I det här exemplet ser vi till exempel en annotering för begreppet CNC-operatör. Vi ser här hur denna annotering innehåller begreppets id, typ och en etikett som är "CNC-operatör/FMS-operatör". Vi ser även en referens till *var* någonstans i texten begreppet förekommer i form av värdena `start-position` och `end-position`. Jag har ritat dit en pil som pekar på den förekomsten.

Detta är alltså ett av de dokument som sommarjobbarna annoterade. Alla dessa dokument lagras som sagt i ett git-repo på nätet och adressen till detta repo hittar man här. I första hand använder vi dessa data internt för att evaluera och träna algoritmer, men de är också allmänt tillgängliga så om någon annan skulle vilja använda dessa data kan de göra det.

Nu tänkte jag visa hur vi använder dessa data för att mäta hur bra en algoritm fungerar.

## Mäta hur bra en algoritm fungerar 16:16

Här har jag försökt illustrera hur det går till när man utvärderar hur bra en algoritm för begreppsigenkänning fungerar.

Till vänster i bilden har vi vår datamängd som består av annoterade dokument. Varje annoterat dokument består i sin tur av en text och begreppsannoteringar för den texten. I mitten av bilden har vi den algoritm som vi vill utvärdera. Som indata till den algoritmen skickar vi in texten och vi får utdata i form av annoteringar. Dessa annoteringar jämförs sedan med de manuella annoteringarna för samma dokument, och på så vis kan vi beräkna ett mått på hur väl algoritmens annoteringar överensstämmer med de manuella annoteringarna för ett dokument.

Vi använder ett mått som benämns Jaccard-index för att mäta överensstämmelsen mellan de två mängderna av annoteringar. Detta är ett värde mellan 0 och 1 som vi uttrycker som ett procentvärde, där ett högre värde är bättre och betyder att de två mängderna stämmer bättre överens.

I nästa bild ska jag förklara mer hur den här beräkningen går till.


## Jaccard-index 16:17

Här har vi en illustration av hur man beräknar Jaccard-index. Det är ett mängdteoretiskt begrepp.

Syftet med det här måttet är att kunna jämföra olika algoritmer för begreppsigenkänning och välja den bästa algoritmen.

Vi har alltså en mängd T av manuellt annoterade begrepp och en annan mängd P av annoteringar som algoritmen, som vi vill utvärdera, har producerat. Vad Jaccard-indexet gör är att det mäter överlappet mellan de här mängderna genom att dela storleken på snittet med storleken på unionen. För enkelhets skull struntar vi i *var* någonstans i texten ett begrepp hittades.

Låt oss ta ett litet exempel. Varje punkt i det här diagrammet motsvarar ett begrepp. I snittet av de två mängderna hittar vi begrepp som både annoterades manuellt och maskinellt av algoritmen. Snittet består av två begrepp varav det ena är CNC-operatör. Den hela mängden av alla begrepp är 10 element. Så då blir Jaccard-indexet 2 genom 10, d.v.s. 20% i det här exemplet.

OK, nu ska vi titta lite mer praktiskt på hur vi använder det här.

## Implementation av begreppsigenkänning 16:18

Här har vi en skiss över hur vi har strukturerat vår implementation av begreppsigenkänning. Vi har byggt ett Python-bibliotek, conrec-utils, som är som en verktygslåda för att arbeta med begreppsigenkänning. Conrec är bara en förkortning för "Concept recognition", d.v.s. begreppsigenkänning översatt till engelska. Detta bibliotek kan användas för att arbeta med datamängder av annoterade dokument. Det implementerar även Jaccard-index för att evaluera algoritmer. Slutligen finns även kod för att presentera resultatet av algoritmen på ett lättöverskådligt sätt, så att man kan förstå vad algoritmen gör.

Baserat på denna kod har vi sedan byggt en algoritm för begreppsigenkänning. Vi kallar algoritmen jae-conrec. Denna algoritm använder sig av JobAd Enrichments för att hitta ord och kopplar sedan dessa ord till begrepp i taxonomin.

Tanken med conrec-utils, alltså den här verktygslådan, är att göra det lätt att komma igång och prototypa nya algoritmer för begreppsigenkänning. Det underlättar också för andra som vill experimentera med begreppsigenkänning på våra dataset.

Nu tänkte jag att vi skulle titta lite på resultatet av att köra algoritmen jae-conrec på ett dokument.

## Begreppsigenkänning jae-conrec

https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec/-/tree/master/data/results/datasets/mentor-api-prod__splitclassvalidation/items/0e19694138c5764ea6238f7c1160b1d04fe7edd4

Här har vi ett exempel på hur resultatet från en begreppsigenkänningsalgoritm kan se ut.

Men jag tänkte att vi skulle titta på det nu mer i detalj.

Resultatet av begreppsigenkänningsalgoritmen är en rapport i form av en webbsida. Här har vi en sådan rapport för ett dokument som vi körde algoritmen på och sedan jämförde algoritmens resultat med manuella annoteringar för dokumentet. Baserat på den jämförelsen beräknade vi sedan två olika sorters Jaccard-index, som vi redovisar här.

Det vänstra jaccard-indexet, "charwise jaccard", är beräknat på ett sätt som tar även positionen i texten i beaktande. Här får vi ett Jaccard-index av 62%, vilket är ganska bra.

Det högra jaccard-indexet, "conceptwise jaccard", är beräknat endast baserat på vilka begrepp som hittades, oavsett var i texten begreppen hittades. Det här Jaccard-indexet är 73%.

Vilket av dessa två mått som är mest relevant beror på tillämpningen.

Sedan, i avsnittet "Source text", ser vi texten som analyserades.

Därefter följer en tabell som redovisar i detalj en jämförelse mellan manuella sanna annoteringar och de annoteringar som algoritmen producerade. Låt oss betrakta den tabellen lite mer i detalj.
Vi kan börja med att titta på den översta raden, där det står: "Empleo söker nu Montör/staplare till Kund i Vislanda". Vi ser att ordet "montör" är skrivet i fetstil. Detta markerar att ordet är ett potentiellt begrepp. I nästa kolumn, "Truth", ser vi ett kryss. Det betyder att det här begreppet är en manuell annotering. I kolumnen därefter, "Predicted", ser vi också ett kryss. Det betyder att även algoritmen producerade detta begrepp just här. Med andra ord stämmer algoritmens annotering överens med den manuella annoteringen. Själva begreppet som montör är kopplat till ser vi längst till höger. Det är ett begrepp av typen "ssyk-level-3", med etiketten "Montörer". Det är alltså en yrkeskategori i det här fallet kan man säga.

~~Vi kan titta på en annan rad:
På den här raden står det "Mäta, packa, montera och banda lameller och spolar". Ordet i fetstil är "banda" och i högerkolumnen ser vi begreppet "Banda" av typen "language". Här har nog algoritmen gjort fel, för om du tittar i kolumnen för "Truth" så är det inget kryss där utan bara ett kryss i kolumnen "Predicted". Det betyder att algoritmen producerade den här kopplingen mellan ordet banda i texten och språket "Banda", men det finns ingen manuell annotering för den kopplingen.~~

~~Detta är alltså ett sätt att presentera algoritmens resultat i detalj.~~

Baserat på alla dessa annoteringar kan man då beräkna ett jaccard-index som är 62%. Och den beräkningen är ganska enkel.

~~Vi kan också titta som hastigast på tabellen under här. Denna tabell redovisar annoteringar av begrepp oberoende av var i texten begreppen förekommer. På motsvarande sätt ser vi t.ex. att en annotering för begreppet "Montörer" finns både i de manuella annoteringarna och algoritmens annoteringar. Däremot finns begreppet för språket "Banda" endast bland de begrepp som algoritmen producerade. Och utifrån denna tabell kan man då beräkna ett Jaccard-index på 73%.~~

~~Så detta är bara ett litet exempel på vad vi håller på med inom begreppsigenkänning. Jag tänkte att vi skulle beröra lite svårigheter med begreppsigenkänning.~~

Nu har vi sett ett smakprov på hur begreppsigenkänning fungerar. Jag tänkte runda av lite nu med fortsatt arbete

## Fortsatt arbete

Det vi i första hand har framför oss är att färdigställa och dokumentera verktygslådan conrec-utils, så att den är lätt att använda för nya projekt. Detta ökar också möjligheten för andra att experimentera och evaluera algoritmer på de data som vi har samlat in.

Sedan vill vi förstås också bygga bättre algoritmer för begreppsigenkänning. De resultat som vi får med den nuvarande algoritmen som jag presenterade är ju knappast perfekta, det finns nog mycket där att förbättra.

Något som vi är nyfikna på är att titta mer på hur vi kan använda så kallade artificiella neurala nätverk, som till exempel BERT, för begreppsigenkänning och att producera bättre resultat.

Det var nog allt jag hade att säga om begreppsigenkänning. Då lämnar jag ordet över till någon annan.

# Feedback

**Henrik**:
Det är 15 ~~20~~ minuter.
Spar presentationen, den är bra. Visa för Johan.

**Sandra**:
Det är lite långt
Försök korta ner det tekniska

**Sara**:
No feedback

**Jonas**:
Utelämna bilder om kopplingar till andra strukturer.
Repa om och om igen, tills det flyter bra.
