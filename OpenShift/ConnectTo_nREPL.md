# How to Connect to nREPL in a Pod Running in OpenShift

This tutorial is based on this webpages:
- https://blog.openshift.com/using-clojure-on-openshift/
- http://www.luminusweb.net/docs/repl.html

NOTE: Not all services supports this!

## In OpenShift

1. Set the enviroment variable NREPL_PORT to 7888 (or whatever you like), under a pods Environment tab
2. Save and Action -> Start roll out

## In Command Line

1. Log in to oc with the oc command. (use the copy login command from your project)
2. Switch to your project 
   ```
   oc project taxonomy-api-gitops
   ```
3. To get your pod name run
    ```
    oc get pods
    ```
4. Port forwad to you pods nrepl port
    ```
    oc port-forward <pod name> 7888:7888
    ```

## In Emacs

1. Run
   ```
   M-x cider-connect
   127.0.0.1
   7888
   ```
2. Navigate to your namespace that you want to use
3. Load namespace in repl
   ```
   C-c M-n n
   ```
