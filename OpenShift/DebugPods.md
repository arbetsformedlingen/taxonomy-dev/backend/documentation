# Debug Pods in OpenShift

From these [instructions](https://blog.openshift.com/oc-command-newbies/)

1. Get login token from OpenShift GUI. Click your username and “Copy login command”
2. Run command in terminal
  ```
  oc projects
  oc project YOUR_PROJECT_NAME
  oc get pods
  # get a shell inside a pod for dc/jorge
  oc debug dc/jorge
  # as root user
  oc debug --as-root=true dc/jorge
  # or using the pod name
  od debug pod/<pod name>
  ```
