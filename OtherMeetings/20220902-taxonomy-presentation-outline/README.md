# Presentation

Overview:

* Introduction of the team: 5 minutes
* Henrik presents the essence of what the taxonomy is and why we need it: 15 minutes
* Sara presents demo *suggest-concept-given-string*: 10 minutes
* Jonas talks about NLP and some projects: 15 minutes
* Sandra on FreeSearch Explorer: 10 minutes
* (Questions) : 5 minutes

Each one makes their slide show, but we help each other with slides:

* Jonas produces slides/talk for Henriks part about API.

## Introduction of the team

Everyone presents themselves in a few words to Therése.

* Henrik
* Jonas C
* Jonas Ö
* Sandra
* Sara
* Sruthi

## [Henrik] Why we need the taxonomy (Background)

See the picture ![Chat conversation about importance of taxonomy](why_we_need_taxonomy.png).
IT SAYS IT ALL!!!

We use the taxonomy to encode job ads in Platsbanken.

To do:
* Show a job ad, in platsbanken. Show screenshot.
* Then show the data of the job ad.
  Henrik sent some example: https://mattermost.jobtechdev.se/batfish/pl/hk7qd3yymfryig59imkb1y1hir

we use it everywhere in Arbetsförmedlingen.

We can reuse material from a previous presentation about the purpose of the taxonomy: [kll_presentation_taxonomin.pdf](../../presentations/20220216-kll-seminar/kll_presentation_taxonomin.pdf).

For instance, show the picture "Taxonomy inom Arbetsförmedlingen".
List other systems, and their purposes.

Good if we can explain the purpose of the taxonomy and why we need it, by showing examples. But we cannot show a complete list.

**See the section about införandeprojektet**. Nevertheless, show some motivating examples from how the taxonomy is applied and used to code information in various systems at AF.

### [Henrik] The Atlas (what does the Taxonomy look like)

(Navigate around, screen share)

## [Henrik] What the taxonomy is (Technical)

It is a database of **concepts** and **relations** between concepts.

What is a concept?
It consists of
- an id (very important)
- a preferred label
- a type
- alternative labels (can be none)

What is a relation?
A directed link from one concept to another concept (or the same concept) and type of relation.

What we can show here:
* Screenshot of the Atlas
* Raw data published on data.jobtechdev.se (how it is coded in, say, JSON).

### [Henrik] APIs [Jonas prepare slides and talk, send to Henrik]

Explain that this is how other systems can consume the taxonomy.

Demonstrate Swagger with GraphQL-queries and the fancy query builder interface.

### [Henrik] Versions

We have a versioning feature that lets us manage updates of the taxonomy in a sensible manner...
Explain how versioning works.
Just like Git. In short: We can see what the taxonomy looked like at any point in time in history. Useful for older systems who are not robust to updates of the taxonomy. They can just stick with an old version.

Maybe show versions in the GraphQL API.

~~[Henrik] (The editor?)~~
~~Mention that we have the other team responsible for the data.~~

## Ongoing work

### [Henrik] Införandeprojektet

**About**: Getting others within AF to use the taxonomy.

Recall:
See the picture ![Chat conversation about importance of taxonomy](why_we_need_taxonomy.png).
IT SAYS IT ALL!!!

It is about getting all the systems in Arbetsförmedlingen to use the latest and greatest version of the taxonomy.

Examples of places where we want to use the taxonomy.

https://confluence.arbetsformedlingen.se/pages/viewpage.action?pageId=65737267

See ![Image of AF systems using taxonomy](inforande_tax.png). More than 20 products.

Hard to decide on a date when it should be finished.

"Inskrivningen", she couldn't find her *occupation name*. A place where it is used. But it is also used in AIS.

We are releasing new versions of the taxonomy all the time, but other systems use older versions of the taxonomy and they have difficulties in updating their system to use the new taxonomy. Some systems are hard because they are complex, and others prioritize other things and not the taxonomy.

### [Sara] suggest-concept-given-string (is there a better name for it?) (semantic concept search?)

It is **OK to speak English** for this part.

Few slides explaining how it works based on SBERT embeddings.

Make a small demo?
Type something into swagger and see what concepts come up. For instance, type "terrorist" and see 4 yourself.

We need to come up with good examples.

But it is work-in-progress.

### [Jonas] Acquisition of machine learning data : Annotation of job ads and education descriptions

* Show the annotation editor and how it is used to mark up concepts in text.

* Explain the usefulness of this:

  - Training/testing machine learning algorithms for recognizing concepts in written text
  - Training/testing machine learning algorithms that can establish connections between *skills* and *occupations* (or occupation training)

In short, this data is very valuable in general.
- Finding new words not currently in the taxonomy
- Establishing new connections between words
- Giving more context of how taxonomy concepts are being used in reality.

### [Jonas] Concept recognition

Algorithmic prototype of finding concepts in text.

Present a bit about our work and the challenges, for instance, the meaning of a word from its context. See for instance 1abfa5e75fbc1d6a2601a9989638a16125d6eb60 and how "sjuksköterskor" could map to occupation-name with label "Sjuksköterska, grundutbildad" or "Sjuksköterska barnavårdscentral".

Present this work in a nice way that is easy to follow.

Why is this useful?

- we can match, for instance, an education with a job ad?
- we can cluster similar ads
- job links: that data is not tagged.
- Searchability/indexing/information retrieval? Find documents containing a concept, or broader concepts.

In short, it helps to connect meanings between different pieces of data by being able to express each piece in terms of taxonomy concepts.

Show diagram of software components, `conrec-utils` and `jae-conrec` and explain that this makes it easy for others to contribute algorithms.

### [Jonas] Involvement in semantikprojektet

* Making the taxonomy available on dataportalen (DIGG)

Show some screenshots from Dataportalen.

## Future work

### [Jonas] Establish connections

between the taxonomy and other terminologies. Building the actual connections is the job of Redaktionen.

We can build tools to help them. Show example of the green-skill mapping maybe?

### [Sandra] FreeSearch Explorer

Samla in fritextsökningar i en databas.
Demar det kanske.


## Time for questions

Therése can ask questions.
