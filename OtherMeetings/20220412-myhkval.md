# 2022-04-12 -- MYH Kvalifikationsdatabasen

## Deltagare

* Peter Böös, MYH
* Jon Findahl, JobTech
* Christer Niklasson, JobTech
* David Norman, JobTech
* Petter Nygård, JobTech
* Klara Nästlin, MYH
* Jonas Östlund, JobTech

## Vad sades på mötet?

**Klara Nästlin**: Sist pratade vi om utmaningar:
* Yrkesklassificering på kvalifikationer
* Kräver ISCO-SUN kvalificering enligt EUROPASS
Finns det någon mappning för automatik? Verkar inte som det. Vi går vidare med att lägga på en SUN-kodning i antingen (i) ansökan eller (ii) handläggaren för nivåplacering på myndigheten.
Idag finns begrepp (i) verksamhetsområden och (ii) kvalifikationsområden, men vi kommer använda era begrepp där (yrkesklassificering).

Om det är kvalifikationer från utbildningsväsendet blir det omvänt: Man har SUN men inte yrkesklassificering.

Vi kommer att kalla "yrkesområde=yrkesområde" , yrke="yrkes/kompetenser". Vi håller på med utvecklingen nu intensivt.

Tanken är att kvalifikationerna ska finnas på MYH.se men även på externt API innan sommaren.

Vi hade några frågor, eller hur Peter? Gällande SSYK-4

**Peter**: Jag har en fråga gällande källan. Den pekar på en JSON-fil vilket är helt OK. Min fråga är: Är det bäst att går rakt mot en fil? Eller finns det bättre vägar.

**David**: De uppdateras per automatik när vi släpper en ny version. Men man kan få ut det på andra sätt också, t.ex. GraphQL.

**Peter**: Jag har kikat lite men har för lite kunskap för att sätta ihop en fråga. Klara: Vad är det för nivåer vi pratar om (två stycken va?): Yrkesområde och Nivå 4.

**Klara**: All information finns i JSON-filen.

**David**: Man kan förvänta sig uppdateringar varje månad, men inga stora grejer när det gäller de här grejerna.

**Jonas**: Jag behöver kolla med Joakim om JSON-filerna avspeglar tillståndet i taxonomin.

**Peter**: Vi kommer väl göra som mest en gång per dygn, eller kanske veckan.

**David**: Hur används det? Är det en handläggare hos er som ska välja ett yrke. Men också befintliga, flagga om vi faktiskt kan byta?

**Peter**: Vi kommer ha värden i dropdown-listor.

**Klara**: Det verkar inte vara något allvarligt som skulle kunna hända såvida vi inte har otur.

**David**: Vi har lite olika sätt att benämna olika yrken. Ibland kan det vara att man kommer in med en specifik titel, en företagsspecifik titel nästan. Dessa kopplar vi till yrkesbenämningar. Det kan vara senare att man kanske vill ha någon sökhjälp.

**Klara**: Vi tänker oss att de se innehar kvalifikationerna också ska få administrera dem. Därför är det bra att veta att sökhjälp finns.

**Petter**: Vi tittar på kvalifikationerna och har dem som källa. De är ju nivåplacerade och håller hög kvalité. Om man t.ex. inte hittar ett bra yrke att koda in det på kan ni bolla med oss?

**Klara**: Jag tar med mig det. Och ser till att de som jobbar med det praktiskt inte ska tveka att ha en dialog med AF om det finns oklarheter. Hade vi något mer, Peter? Petter, du skickade listan där man ska koppla något?

**Petter**: Det är ett pilotprojekt. Man ska skicka in 40 kvalifikationer och läranderesultat och ett verktyg som de har utvecklat kommer att ge förslag på motsvarande ESCO-begrepp. Någon gång efter påsk kommer det igång.

**Klara**: Det var alltså den listan ni skickade?

**Petter**: Ja, och det var du som gjorde grovjobbet, David.

**Klara**: Visar admingränssnittet:

![bild 1](20220412-myhkval/bild1.png)
![bild 2](20220412-myhkval/bild2.png)
![bild 3](20220412-myhkval/bild3.png)
![bild 4](20220412-myhkval/bild4.png)

**David**: Skulle vara användbart med något ännu mer detaljerat. Varje lärandemål för sig. Men vi är ju en ganska liten grupp.

**Klara**: Vi tar det stegvis. Bryter ner det så att (i) data är dat och inte bara är en pdf och (ii) sedan bryter vi ner det i delkvalifikationer. Man kanske kan bryta ner det i ytterligare en nivå. Har ni något annat som ni gör som vi bör känna till?

**Petter**: Gällande mikromeriter är det något uppenbart vi bör känna till?

**Klara**: På EU-nivå tänker man sig att ackrediteringar som man skickar med kvalifikationerna ska kunna kopplas till microcredentials för att se att det är på riktigt men vi kommer i första läget inte skicka det utan ta det stegvis istället. Vi ser att möjligheten finns men kommer inte skicka delkvalifikationer än. Det handlar om att man från EUs sida vill kunna ha olika ISCED kod och SeQF. Också kopplat till microcredentials. Jag kanske ändå kan boka in en timme igen innan sommaren? Då kanske vi har något att visa.

## Sammanfattning

* Petter nämnde projektet med koppling av läranderesultat mot ESCO.
* Vi pratade om mikromeriter
* Klara nämnde olika kodningar som används för kvalifikationerna.
* Jonas ska kolla hur ofta JSON-filerna uppdateras. SVAR: En gång per dygn körs ett cron-jobb, men frekvensen kan ökas på begärasn.
* MYH:s handläggare får gärna bolla med JobTech angående yrken som saknas.
* MYH:s kvalifikationer kommer att finnas på MYH.se och som ett API innan sommaren.
