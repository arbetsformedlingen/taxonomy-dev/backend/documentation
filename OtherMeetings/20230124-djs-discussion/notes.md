# 2022-01-24 &ndash; Diskussion om data.jobtechdev.se

Det här mötet handlade om att göra om hur vi publicerar taxonomin på data.jobtechdev.se och hade ett [tillhörande bildspel](pres.pdf) med överblick och förslag. Se det bildspelet för detaljer kring förslagen.

Joakim klargjorde lite för nuläget och framtiden:
* Just nu är vi hårdlödda in på S3
* CDN
* OK att duplicera kataloger, t.ex. katalogen `latest` kan vara en duplicering av katalogen med den senaste versionen.

Henrik ansåg att senaste versionen ska ligga under `version/latest/` istället för bara `latest/`.

Henrik hade några andra synpunkter också:

* Ascii-kompatibla url:er.
* Namn på engelska gör det lättare att undvika tecken som å, ä och ö som kan orsaka problem med kodning.
* Försök se det utifrån en användares perspektiv.

Joakim hade ytterligare några :

* Varje katalog bör ha en index-fil: ~~index.html~~. Per meddelade efter mötet att det ska vara en fil med namnet `README` som innehåller markdown-kodat innehåll. 
* Låta gamla filer ligga kvar på data.jobtechdev.se efter dessa ändringar, för bakåtkompatibilitet.

Sedan följde en diskussion om namn och filstruktur.

Johans förslag:

Johan och Joakims förslog följande:

* `version/9/query/all-concepts` <-- för varje rad i csv-filen får vi en katalog.
* `version/9/query/all-concepts/index.html` <-- Förklarar vad vi frågar API:et.
* `version/9/query/all-concepts/all-concepts.json` <-- Svaret från API:et. (istället för `data.json`, blir namnkollision om man laddar ner olika filer).

Sedan blir SKOS:

* `version/9/skos` <-- Huvudkatalog för alla SKOS-filer från 9:e versionen av taxonomin.
* `version/9/skos/ttl` <-- Turtle-format av skos-exporten.

## Sammanfattning

* Låt gamla data ligga kvar ett tag till.
* Generera `index.html` för varje katalog.
* Exporterade filers namn på *engelska*, inte *svenska*.
* Använd `version/latest` istället för bara `latest`.
* Implementera Joakims/Johans förslag med `query`.
* Exportera endast *råa* SKOS.

