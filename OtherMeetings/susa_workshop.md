# 2022-04-06 -- Workshop kring behovet och önskat resultat kring SUSA-navsdata och kursinformation.

## Deltagare

* Stefan Eck
* Gregory Golding
* Carl Lernberg
* Olle Nyman
* Mattias Persson
* Susanne Rönnemark Nording
* Henrik Suzuki
* Jonas Östlund

## Ungefär vad som sades

**Carl**: Koppla utbildningar och kurser på lärosäten. Finns mer beskrivande text på hemsidorna än i utbildningsbeskrivningarna.

**Henrik**: Inget specifikt mot SUSA-navet. Ta fritext och hitta begrepp i den.

**Carl**: Datakvalitéen i SUSA-navet inte bra, dubletter av kursbeskrivningar. Det saknas 20% i utbildningsbeskrivningar.

**Olle**: Vi har tre olika uppdrag/utmaningar/behov. Min ingångsvärden som vi delar med MDU och Learning for Professionals.
* Titta på datamodellen för hur en kort utbildning ska se ut som riktar sig mot yrkesverksamma. Vi struntar i Arbetsförmedlingen för att få en lägstanivå av vilka värden som behövs för learning for professionals.
* Hela taxonomibegreppet. MYH, kompetenspasset. Hur skulle man kunna hantera data för att speca ner taxonomin.
* SUSA-navet.
    
**Stefan**: Angående Learning for Professionals. Hur vet man om en kurs i SUSA-navet är lämplig för en yrkesverksamhet?
Och sedan: SUSA-navet har inte all information. Om  man ska presentera data för en yrkesverksam, kommer SUSA-navet har den informationen inom överskådlig tid? Frågan är om vi ska bygga på SUSA-navet eller bygga något annat system som hämtar information någonannnanstans. Är SUSA-navet ett problem.

**Olle**: SUSA-navet är en lågt hängande frukt. Vad är miniminivån för att publicera data på SUSA-navet? Vad måste finnas med? Vad publicerar man i kurskatalogen?

**Stefan**: Vilka objekt ska vi hantera. Vad finns det för utbildningsobjekt som en yrkesverksam vill ha? Ska vi ha MOOC:ar och seminarier också i SUSA-navet. Uppdragsutbildningar får man inte lägga i SUSA-navet.

**Susanne**: Jag vet inte vad SUSA-navet tillåter eller ej.

**Olle**: Så länge det inte dyker upp där man söker högskolekurser så är det inget problem.

**Susanne**: På Ladok blandar vi aldrig ihop en uppdragskurs med en uppdragskurs.

**Carl**: Det finns ju fält som beskriver om det är folkhögskola, gymnasium, etc, men sedan får man ju filtrera. Att det där med att identifiera huruvida en utbildning är lämplig för yrkesverksamma kan man också använda maskininlärning till. Medan ni ser det som att fälten ska struktureras, och det är det bästa, men det ställer också krav på dem som ska skicka in data.

**Olle**: En parameter är ju hur länge man kan ta ledigt från jobbet. Pace-of-study och omfattning är ju väldigt relevant. Med omställningsstödet blir som är 1 år och kortare relevant.

**Stefan**: Orten spelar också roll. Det som passar för en person behöver inte passa för någon annan.

**Mattias**: Distansstudier och helgstudier är också relevanta för en yrkesverksam.

**Carl**: Hur långt har det tagit tidigare från att man efterfrågat ett fält tills att folk börjar skicka in data. 1 månad är ingen tid alls. Men i annonseringsvärden är fem år ingen tid alls.

**Stefan**: Det var det jag menade: Är SUSA en möjlighet eller problem. Hur lång tid tar nya funktioner.

**Greg**: Vi pratar om tekniska förutsättningar. Det vi pratar om gällande värdet av SUSA handlar också om beteendet och människors förmåga att förhålla sig till en centraliserad lösning. Jag är tveksam till att bygga flera åtgärder på en centraliserad lösning som SUSA. Jag tycker att det är naivt att basera tekniska lösningar på SUSA. Carl och Mattias hade ingen partisk ställning till SUSA och det var en lågt hängande frukt. Vilka problem vill vi lösa? Vi kommer till frågor om datakvalité, m.m.

**Olle**: Med tanke på att vi äger hela produktionskedjan i det här mötet kan det vara värt att lägga lite tid på det. Vi kanske kan skära bort onödig förvaltning. Vi har alla relevant aktörer närvarande så det kan vara värt att testa innan man lägger tid på att uppfinna något annat.
De kurser som är relevant för målgruppen är kanske kortare poängsatta kurser (skippa MOOC:ar). Kolla om de håller tillräckligt bra kvalité för att kunna publiceras.
Hela datamodellen kan vi prata ihop oss om. I dagsläget har man inga tvingande fält, så man kan publicera helt tomma kurser.
Jag behöver prata vidare med Revival och Taxonomy om digital infrastruktur-jobbet.

**Mattias**: Vi har olika fokus. Du vill presentera saker, vi vill matcha.

**Carl**: Och sedan att vi har approachen att vi matchar baserat på maskininlärning av fritext. En kurs i SUSA-navet kan vara några veckor och ett program kan vara 5 år, men det är bra om man vet vad de fem åren ska handla om.

**Olle**: MYH jobbar å sin sida med att beskriva kurser med ”aktiva verb”, d.v.s. t.ex. när man har gått en kurs: ”efter kursen ska du kunna…”.

**Carl**: Jag och Mattias har som hypotes gällande matchning. Yrkeshögskolor är arbetsnära: nära koppling mellan utbildning och framtida jobb. Men däremot högskoleutbildningar och studieförberedande gymnasierprogram, där är kopplingen lösare. Det är forskningsförberedande. Sedan råkar det finnas ett sug efter högskoleutbildad personal. Men den matchningen handlar nog mycket om tillfälligheter, slumpen, kontakter. Svårt att komma med en exakt sanning och mappa. Stor skillnad mellan aktörer inom KLL och förutsättningar för att matcha. Stort utrymme för slumpen, vi kallar det för rekommendationer och förslag.

**Olle**: Det blir spännande ur prognosperspektiv.

**Carl**: Det är ju lite som vädret.

**Olle**: Har ni testat matchningen? Hur dåligt blir det?

**Mattias**: Detta är ett första utkast. Vi skulle kunna köra demo någon gång. Det är hyfsat bra resultat och det är helt automatiskt.

**Olle**: Vi skulle gärna se en demo (mina utvecklare och jag)

**Mattias**: Från yrken till utbildningar är det ännu bättre. Det kan vi också dema.

**Carl**: Vi har sorterat efter relevansordning. Topp 10 brukar vara bra. Det kan variera med inriktning. Ofta är det väldigt klockrent.

**Stefan**: Vad använder ni för yrkesnivå?

**Mattias**: Vi använder yrkesbenämning (finare nivå än SSYK). Det är vad arbetsgivaren sätter när de publicerar det.

**Carl**: Eftersom det är mer finkornigt får man ju SSYK också.

**Olle**: Jag är nöjd nu.

## Sammanfattning

* Vore bra med en fortsatt diskussion mellan RISE och JobTech (Revival och Taxonomy)
* RISE, MDU och Learning for Professionals ska titta vidare på vilka fält som bör vara minimikrav för att publicera på SUSA-navet
* Revival skulle kunna dema sin utbildnings-/jobbannonsmatchning när de känner sig redo.
