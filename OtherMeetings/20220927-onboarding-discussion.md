# Meeting to discuss onboarding of new employees

## Initial discussion about the onboarding

Henrik:
We should help each other out, work on each other projects. Help Johan get things running. We should go through our stuff and get it running on their computer.

Break down what we are going to work on into tasks, and let them pick issues. We need to share more knowledge. It takes more time to interact, but it is.

Jonas:
Good to prepare some beginner-friendly tasks that are easy to understand.

Henrik:
Present them the roadmap. They should be able to choose themselves, to be self-directed.

Jonas:
I should share the work I did after the summer on the datasets and jobad enrichments, in the spirit of facilitating collaboration in the team. Although it is written in Python and we advertised Clojure in the job ad...

Should we make a rough plan for onboarding them?

## Rough planning

1. Introduce them to the other teams. All the teams, join their morning meetings.
2. Refer to the document [OnBoarding.md](../OnBoarding.md) where they find information that they need.
3. Present what we presented for Therése. Explain the idea of self-governance.
4. Help them set up their development environment. All should join this meeting.
5. Show them the roadmap, and reconnect to the idea of self-governance: we defined this roadmap.
6. Show them the issues, let them pick. Prepare some easy-to-understand task. What tasks?
