# Discussion with Metasolutions/DIGG about SKOS

Regarding exporting the Taxonomy to SKOS:
* Concept *types* in the taxonomy are represented as *scheme* in SKOS. Every concept type gets defined as a scheme.
* Every *scheme* has a set of top concepts. A top concept is a concept that does not have a broader concept.
* Don't use `rdfs:label` for scheme, use `dcterms:title` instead. A `dcterms:decription` would be great as well.
* Use broadMatch to reference across different schemes.
* Use language tags with preferred label, etc.
* Use one file per scheme.
* Visit editera.dataportal.se
