# Möte med Kandidatbanken i Solna om migrering av inaktuella taxonomibegrepp

* *Mötesdatum:* 13 februari 2025
* *Plats:* Huvudkontoret Solna, våning 4, hus 1, öppna mötesutrymmet.

![Anteckningar på tavlan](migrerabegrepp.jpg)

## Sammanfattning

Andy presenterade problem med koppling till att gå upp till ny version i deras produkter. I korthet behöver de enlösning för att mappa om inaktuella begrepp till aktuella begrepp.

*Nästa steg:*

* JobTech kommer att producera en json-fil, för varje taxonomiversion, som publiceras på en webbserver. Json-filen för en viss taxonomiversion ska innehålla en mappning från inaktuella till aktuella begrepp som ska användas istället. **Uppdatering:** *se avsnittet *Prototyp* nedan.
* Vi bör se om det finns fler begrepp som kan mappas.

## Prototyp

Koden som implementerar publicering av översättning från inaktuella begrepp hittas här:

https://gitlab.com/arbetsformedlingen/www/data-jobtechdev-se/djs-tax2skos/-/merge_requests/114

Här är resultatet för version 24 i testmiljö:

* I json-format: [`data.json`](https://data-develop.jobtechdev.se/taxonomy/version/24/deprecated2current/data.json)
* I csv-format: [`data.csv`](https://data-develop.jobtechdev.se/taxonomy/version/24/deprecated2current/data.csv)

## Närvarande

* Andy Ishak, Kandidatbanken
* Jonas Östlund, JobTech
* Petter Nygård, JobTech 

## Överblick av system

Andy presenterade de två delsystemen som de arbetar med.

* "Kandidatbanken": Arbetssökande ska skriva in begrepp.
* "Sök kandidater": Arbetsgivare

## Nuläget, taxonomiversioner och aktuella program.

Andy förklarade att för olika typer av begrepp använder de i dagsläget olika versioner.

* Geografiska begrepp: version 7
* Yrkesbenämningar: version 16
* Övriga begrepp: version 1

Sedan hade han gjort ett pythonskript som kategoriserade alla begrepp i färgkoder:

* **Gröna:** Exakt samma begrepp, ej inaktuellt.
* **Gula:** Inaktuellt men har `replaced-by`-koppling till aktuellt begrepp.
* **Röda:** Inaktuellt och saknar koppling till nyare begrepp med `replaced-by`. *Exempel:* Sun education field.

För Sun education field har SCB har översättningsnyckel.

## Problem

Kandidatbanken har data i en databas över kandidater. Databasen innehåller begreppsid:n som var aktuella då datan lades in i databasen men nu är de inaktuella (d.v.s. markerade med `deprecated=true`). De behöver en strategi för att mappa om inaktuella begrepp till aktuella begrepp för en given version.

# Lösningsförslag

Jesper har tidigare efterfrågat en centraliserad lösning från JobTech. Vi diskuterade en lösning som, givet ett *begreppsid* och *versionsnummer* för en taxonomiversion där det begreppet är inaktuellt så får man det aktuella begrepp som ska användas istället.

Vi frågade oss om det är nödvändigt att känna till versionen av taxonomin som det inaktuella begreppet mappas *från* men det behövs nog inte. Vi frågade oss också hur vi hanterar situationer då det inte finns en entydig mappning. Helst bör vi svara med ett entydigt aktuellt begrepp. Men om det inte är möjligt kan vi svara med flera möjliga begrepp. Eller ett förslag på en lista som begrepp kan hittas i.

Kanske ett svar på formen i de fall det finns möjliga ersättningsbegrepp:
```
{"suggestedReplacements": [{"concept/id": "ADF"}, {"concept/id": "aSDF"}]}
```
eller, om det inte finns ett några begrepp att föreslå, åtminstone ett förslag på begreppstyp där ersättningsbegrepp hittas:
```
{"suggestedReplacements": [{"concept/type": "occupation-name"}]}
```
Det enklaste och billigaste är nog att producera en nedladdningsbar fil för varje version av taxonomin.

### Hantering av kedjor av begrepp

I de fall där inaktuella begrepp ersätts av nyare begrepp, kan man tänkas sig att dessa i sin tur ersätts av ännu nya begrepp.

Antag t.ex. att begreppet `ABC` som är aktuellt i version 1 ersätts av begreppet `PKE` i version 2 så att `ABC` är inaktuellt i version 2. I version 3 blir begreppet `PKE` inaktuellt och ersätts av begreppet `BAS`. Och i version 25 blir begreppet `BAS` inaktuellt och ersätts av begreppet `RAT`.

Det betyder att för version 25 ska alla begreppen `ABC`, `PKE` och `BAS` översättas till begreppet `RAT`.

## I form av ett API

Kan vi erbjuda ett API eller någon form av mapping?

```
mapConceptId(srcConceptId, dstVersion)
```

där `srcConceptId` är en sträng med begreppsid, t.ex. `"pKr_S947_kbc"`
och `dstVersion` är versionen som man ska mappa till.

Då ska API:et svara med **ett** begrepps-id, t.ex. `"jjk_M98d_jhd"`.

## I form av filer

Vi publicerar en fil för varje taxonomi-version som vi vill mappa mot.

Filträdet skulle kunna se ut ungefär så här:

```
https://data.jobtechdev.se/
    taxonomy/
        version/
            1/
                replacedBy/
                    README.md
                    data.json
            2/
                replacedBy/
                    README.md
                    data.json

...

            24/
                replacedBy/
                    README.md
                    data.json
               
```

där varje `data.json`-fil innehåller en karta som mappar från alla inaktuella källbegrepp till målbegrepp som ska användas istället:
```
{"pKr_S947_kbc": {"suggestedReplacements": [{"concept/id": "jJZ_O4pr_Mno"}],
                  "ABC": [{"concept/id": "RAT"}],
                  "PKE": [{"concept/id": "RAT"}],
                  "BAS": [{"concept/id": "RAT"}]

                   .....

                  }

....

}
```

## Fler frågor efter mötet

Antag till exempel att vi i version 26 lägger till `replaced_by` för ett begrepp A som blev inaktuellt fr.o.m. version 3. Och att det ersättande begreppet B också är aktuellt i version 3. Vill vi då att detta ska ingå i mappningen också för version 3 eller bara version 26? Vill vi alltså att mappningar ska vara retroaktiva?

## Slutsats

Vi kom fram till att nästa steg är att JobTech försöker producera en första prototyp av det filbaserade lösningsförslaget. Därefter kan Andy/Kandidatbanken börja experimentera med det.
