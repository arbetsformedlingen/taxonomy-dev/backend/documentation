# Migrera begrepp med replaced by

Exempeldatabas:

| Version | cat | rat | bat | hat | mat | fat | Beskrivning |
|---------|-----|-----|-----|-----|-----|-----|-------------|
| 0       |     |     |     |     |     |     |             |
| 1       | 1   |     |     |     |     |     | cat         |
| 2       | d   | 1   |     |     |     |     | cat->rat    |
| 3       | d   | d   | 1   |     |     |     | rat->bat    |
| 4       | d   | d   | 1   |     |     |     |             |
| 5       | d   | d   | 1   | 1   | 1   |     | cat->hat,   |
| 6       | d   | d   | 1   | d   | d   | 1   | mat->fat    |

## En ny endpoint

`GET /v1/taxonomy/main/track-replaced-concept`

Inparametrar:

| Parameter                       | Beskrivning                                 |
|---------------------------------|---------------------------------------------|
| `id`                            | Id för begrepp att mappa från               |
| `target-version`                | Version att mappa *till*                    |
| `max-source-version` (Optional) | Max-version att mappa från, eller `:latest` |

Svar:

Om begrepp och versioner hittas fås svar med följande nycklar och koden 200:
    
| Nyckel              | Beskrivning                                                                  |
|---------------------|------------------------------------------------------------------------------|
| `source-concept`    | En karta med källbegreppets id (upprepat) och vilken version det skapades i. |
| `target-candidates` | En lista med begrepps-id:n och vilka versioner de skapades i.                |

Om något begrepps-id eller någon version inte hittas allt returneras en annan statuskod.

## Exempel 1

Systemet Björnbär har version 1 av taxonomin och begreppet `cat` i sin databas. De vill gå upp till version 3 och vill se om de kan ersätta `cat` med något annat begrepp.

De skickar in
```
{:id "cat"
 :target-version 3}
```
och får som svar tillbaka
```
{:source-concept {:taxonomy/id "cat" :taxonomy/created-in-version 1}
 :target-candidates [{:taxonomy/id "bat" :taxonomy/created-in-version 3}]}
```

## Exempel 2

Systemet Oxelbär har version 1 av taxonomin och får in begreppet `hat` från ett mycket nyare system, Rönnbär. De vill se om de kan använda ett äldre begrepp istället.

De skickar in
```
{:id "hat"
 :target-version 1}
```

och får tillbaka svaret
```
{:source-concept {:taxonomy/id "hat" :taxonomy/created-in-version 5}
 :target-candidates [{:taxonomy/id "cat" :taxonomy/created-in-version 1}]}
```

### Exempel

Vi har ett system som kör version 16. Ett nyare system som kör version 25 skickar begreppet `xyz` och detta begrepp finns inte i version 16.

Vi vill tolka om begreppet version `xyz` till ett begrepp som finns i version 16.

## Exempel 3

Systemet Snöbär har version 3 och begreppet `bat`. De vill migrera till version 4.

De skickar in
```
{:id "bat"
 :target-version 4}
```
och får tillbaka
```
{:source-concept {:taxonomy/id "bat" :taxonomy/created-in-version 3}
 :target-candidates [{:taxonomy/id "bat" :taxonomy/created-in-version 3}]}
```

## Namnförslag

* `migrate-concept-id-to-version`
* `track-replaced-concept`
* `current-for`

| Person   | Namn                                                                   |
|----------|------------------------------------------------------------------------|
| David    | `get-replaced-concept`. Inte helt säker.                               |
| Christer | Inget                                                                  |
| Jon      | Inget                                                                  |
| Jenny    | Inget. Jag som inte kan: `track-replaced-concept`                      |
| Jonas S  | `traceability` och `history`                                           |
| Jonas Ö  | `migrate-concept-id-to-version` eller kanske `concept-id-history`      |
| Johan    | `concept-id-history` inte riktigt vad den gör. `get-replacing-concept` |
| Petter   | Håller med David.                                                      |

Vi väljer att kalla den för `get-replacing-concepts`.

## Diskussion

* Christer: Det är inte säkert att vi vill kunna mappa bakåt. Det är inte nödvändigtvis fel, det kan vara användbart.
* David: Bakåtkompatibilitet. Vi behöver kunna hantera det. Rimligt att kunna följa kedjan åt båda hållen.
* Jon: Det kan bli fel. Utvecklarna kanske är bekväma.
* Johan: Bra om version är med...
* Jonas S: Om man ställer frågan till AIS vilka konceptid:n de hanterar vet de det. Behövs något batchorienterat. AIS vill mest gå över till nya taxonomin. Magnus Kjörling, vi kan ha en sittning med honom.
* Christer: Kan påverka hur vi hänvisar till nya yrken. Lite inne på det Jon nämnde här. Vi får inte bygga för bra kedjor. Du kan inte gå hur långt tillbaka som helst. För mycket spelrum mellan version 16 och version 25.
* Petter: Kan bli fel med delmängd. Ett begrepp som ersätter många äldre begrepp, vi gör alltså *bredare* begrepp.
* David: Ska vi lägga till `replaced-by`-transitivitet i databasen?

## Yrken utan replaced-by

Petter: Behövs det en endpoint för det också?
Johan: Explicit modellera `no-replaced-by`.

Christer: Det finns begrepp som inte går att ersätta med något.
Petter: Finns `VTS- och Lotsoperatör` -> `VTS-operatör` och `Lotsoperatör`. Inte använt `replaced-by` då. Kock är ett annat exempel på begrepp som brutits upp i flera begrepp.

## Övrigt

Behöver enkelt sätt att översätta från konceptid till preferred label.

## Nytt förslag

Ha separata endpoints för att följa kedjor framåt och bakåt.
För att följa kedjan framåt, kan vi

Inparametrar:

| Parameter | Beskrivning                   |
|-----------|-------------------------------|
| `id`      | Id för begrepp att mappa från |
| `version` | Version att mappa *till*      |

Svaret blir en lista med

```
[{:taxonomy/id ...} ...]
```

Namnet blir `get-replacing-concepts.`

## Slutsats

Vi bestämmer oss för att implementera det som är beskrivet under **Nytt förslag**.
