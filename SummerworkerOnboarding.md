# Summerworker Onboarding

Steg-för-steg-instruktioner för sommarjobbare.

Huvudsakliga arbetsuppgifter:
* Annotera jobbannonser
* Annotera utbildningsbeskrivningar

Annoteringsverktyget finns här och kräver ingen inloggning utan kan användas direkt: http://jobtech-annotation-editor.jobtechdev.se/
Se [Routes](https://console-openshift-console.prod.services.jtech.se/k8s/ns/jobtech-annotation-editor-prod/routes) om det behövs.

Det finns också en test-version av verktyget: http://jobtech-annotation-editor-jobtech-annotation-editor-test.test.services.jtech.se/


## Introduktion: Vad sysslar JobTech med

* Visa Atlasen och förklara lite om vad taxonomin är och dess syfte att koda information inom AF: https://atlas.jobtechdev.se
* Visa API:et som används av andra system: https://taxonomy.api.jobtechdev.se/v1/taxonomy/graphiql

## Komma igång med arbetet

* Öppna Chrome eller Firefox
* Skapa ett konto på Mattermost: https://mattermost.jobtechdev.se
* Be någon från Calamari att lägga till rättigheter.
* Kontrollera tt du kan chatta.
* ~~Bestäm dig för ett kort smeknamn som du använder i annoteringsverktygen, t.ex. "grankott3".~~ EJ NÖDVÄNDIGT
* ~~Skapa Gitlab-konto (behövs det nu?)~~
* Gå till sidan för annoteraren: http://jobtech-annotation-editor.jobtechdev.se/
* Du kanske får detta fel:
```
Content Security Policy: The page’s settings blocked the loading of a resource at http://jobtech-annotation-editor-jobtech-annotation-editor-prod.prod.services.jtech.se/favicon.ico (“default-src”).
```
I så fall, försök med den andra webbläsaren, eller läs mer om PEM-certifikat här: https://gitlab.com/arbetsformedlingen/devops/calamari-documentation/-/issues/95

## Övningsuppgift: Annotera en platsannons

* Klicka på "Skapa ny"
* Gå till platsbanken.
* Sök på något, t.ex. "snigel". Du får annonsen https://arbetsformedlingen.se/platsbanken/annonser/26164123.
* ~~Kopiera in texten från annonsen i textrutan i annoteringsverktygen och tryck på "Kör"~~ **Kopiera annonsens URL** till URL-rutan i annoteringsverktyget och tryck på "Kör".
* Gå igenom alla upptaggningar som gjorts automatiskt och ta bort dem som verkar felaktiga, t.ex:
   - skill "positiv"
* Gå igenom texten och tagga upp med de begrepp som passar, t.ex. för "hitta lösningar på problem" finns det en esco-skill som du kan välja genom att trycka på "Ny annotering" och knappen "Ändra" under rubriken Koppla mot. Välj begreppet med id `WFSY_Bwt_QWE`.
* När du är klar med taggningen, tryck på knappen "Ny meta", lägg till "user" och skriv in ditt smeknamn, t.ex. "grankott3".
* Kontrollera att annonsen dycker upp på GitHub: https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/nlp/annotations/mentor-api-prod

Du är nu färdig klar med övningen och redo att gå vidare med en av arbetsuppgifterna.

## Arbetsuppgift 1/3: Annoter platsannonser

Syftet med denna arbetsuppgift är att tagga platsannonser för att använda som träningsdata.

* Gå in på platsbanken, välj "Yrke" och välj allt under ett yrkesområde.
* Annotera en annons i taget med start från toppen i resultatlistan. Gör annoteringen på det sätt som beskrivs under avsnittet Övningsuppgift.

Observera att olika sommarjobbare ska välja *olika* yrkesområden så att de inte överlappar.

## Arbetsuppgift 2/3: Tagga utbildningar från kalkylark

Syftet med denna 

* Ladda ned detta kalkylark: [Utbildningsinformation från MYH 20210909.xlsx](https://s3.console.aws.amazon.com/s3/object/yh-utbildningar?region=eu-north-1&prefix=yh-utb-excell/Utbildningsinformation+fr%C3%A5n+MYH+20210909.xlsx)
* Lägg till en extra kolumn i början med titeln "SHA"
* Hoppa till raden med kolumnvärdet `Utbildningsnummer` = `YH00198`
* Gå in i annoteringsverktyget (http://jobtech-annotation-editor.jobtechdev.se/)
* Klicka på `Skapa ny`
* Kopiera texten från kolumnen `Om utbildningen` från den raden och klistra in i rutan `Från text` i annoteringsverktyget.
* Klicka på knappen `Kör`
* Annotera annonsen.
* Komplettera med metadata:
  - `user = [ditt användarnamn på mattermost]`
  - `type = education-description`
  - `dataset-id = myh-spreadsheet-2021`
  - `dataset-document-id = [Utbildningsnummer]`
  Ungefär så här ska det se ut (`user`-metadatan syns ej i nedanstående bild, men den ska vara med):
  <img src="annot.png" alt="Annotation editor" width="600"/>
* Klicka på `Spara`.

## Arbetsuppgift 3/3: Tagga utbildningar från S3
Se här: https://s3.console.aws.amazon.com/s3/buckets/yh-utbildningar?region=eu-north-1&tab=objects

*DENNA ARBETSUPPGIFT ÄR EJ FÄRDIGDEFINIERAD*

* Välj en utbildning i taget.

## Länkar

* [JobTech Mattermost](https://mattermost.jobtechdev.se): Chattverktyget som används inom JobTech.
* [Taxonomy Atlas](https://atlas.jobtechdev.se): Webbgränssnitt för att utforska taxonomin.
* [GraphQL-API:et](https://taxonomy.api.jobtechdev.se/v1/taxonomy/graphiql): Webbgränssnitt för att bygga GraphQL-frågor och köra dem mot API:et.
* [Annoteringsverktyget](http://jobtech-annotation-editor.jobtechdev.se/): Verktyget som används för att annotera annonser.
* [mentor-api-prod](https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/nlp/annotations/mentor-api-prod): Var annonserna sparas någonstans.
* [https://data.jobtechdev.se/taxonomy/index.html](https://data.jobtechdev.se/taxonomy/index.html): Taxonomin som json-data.
* [begrepp-och-vanliga-relationer.json](https://data.jobtechdev.se/taxonomy/begrepp-och-vanliga-relationer.json): Hela taxonomin som en enda json-fil med de vanligaste fälten och relationerna till andra begrepp.

## How to deploy Mentor API

These instructions are main

This is the backend service that powers the editor.
To deploy to prod or test, simply remove an old prod/test tag and add a new prod/test tag and push. Aardvark will build and deploy. This can be done in Gitlab by clicking somewhere.

To edit tags in Gitlab, go to `Repository -> Tags`. Press on the trash can for the tag you want to remove. Then click the button "New tag" and attach it to e.g. `master` if that's where your changes are.

# Anteckningar

## Sentiment
I platsannonserna 


### Flera yrkesbenämningar i annonsen
Ibland förekommer det yrkesbenämningar i annonsen som inte beskriver vilket yrke man ska jobba som. Till exempel att man jobbar i ett team med psykolog, och sjuksköterska. Tagga dessa yrkesbenämningar med sentimentet "Inte relevant".

## Kollektivavtal
Ibland förekommer det att arbetsplatsen erbjuder kollektivavtal, alltså inte man använder kompetensen kollektivavtal i sin yrkesutövning.
Tagga dessa ord med sentimentet "Erbjudande"

## Skilj på företagsbeskrivning och kompetensbehov
Ibland förekommer det kompetensord i texten som förklarar vad företaget arbetar med.
Till exemepel kan det stå att företaget jobbar med medicinteknik. I detta fall så kan man tagga upp med sentimentet "Inte relevant".
Däremot om det står i samma text att "Du kan medicinteknik", tagga upp ordet med sentimentet krav.

