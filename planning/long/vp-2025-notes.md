## About

The document outlines the current state, desired state (for december 2025), risks and potential collaborations related to the development of the Taxonomy for the verksamhetsplan (VP) within the area of "gemensam begreppsstruktur".  It is the product of a meeting with Taxonomy dev and Redaktionen, where the VP was discussed and the notes below where taken.

Below, each number is tied to the same topic in each list. So topic (1) in "Current state" relateds to topic (1) in "Desired state", and so on.  

**Feel free to update this in any way you want.**

## Current state:

1) Taxonomiinförandet is starting up again - no decisions yet
   on implementation

2) The Taxonomy should contain labour market terminology. Structures that falls outside that domain should, as far as possible, be handled elsewhere. For instance Geographical terminologies.

3) The Taxonomy lacks transparency when it comes to types and how they are defined and relate to each other.

4) Link to current RDF files: [Katalog](https://data.jobtechdev.se/taxonomy/version/22/skos/raw/ttl/index.html)

5) Currently we have a mashup of the documentation, the taxonomy and the delivery system and we are working with splitting them.

6) Currently there are very few descriptions for occupation-name concepts, making it sometimes unclear what they mean. More detailed relations between occupation-name concepts and skills have also been requested by many stakeholders.

7) The Atlas is somewhat lacking in UX, and some functionality needs to be revamped

## Desired state (2025):

1) Taxonomiinförandet is implemented and a new version has been published after the implementation

2) We have found a more fitting ownership for some of the terminologies, for instance geographical (this can be related to the ongoing work with creating a data model for Af)

3) The Taxonomy is transparent regarding characteristics of types, relations etc.

4) Better ways of disseminating the Taxonomy (RDF)

5) Deploying a new taxonomy should not require a new API and
   updating the documentation should not require a new deployment of the API

6) A number of concepts have been enriched with descriptions and more detailed relations (will likely not be finished at this point)

7) Overhaul of the Atlas, like handling of the changelog, and some quirks that are currently in the Atlas code (like certain groupings of concepts) should be handled by the Taxonomy instead.

# Risks:

1.1) Taxonomiinförandet is slow moving. Many decisions need
 to be made that are outside of our control.

1.2) Communicating what the Taxonomy is about is really hard.

1.3) Loss of knowledge, competence and relations with the departing
 of Henrik

# Collaborations:

1) Maybe in-person support for teams within the IT department
   working with the Taxonomy to help with the taxonomiinförande

2) Team working with informationshantering

3) Teams managing end-user facing products, like Mina profiler,
   for testing. Teams in other departments, working with contacts within certain industries. Knowledge heavy external organisations for input.


