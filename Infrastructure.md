# Infrasctructure

This described the high-level infrastructure handled by the Taxonomy Dev team.

## Clusters and High-Level Systems

The Taxonomy infrastructure consists of a couple of components running in the following systems:
- [Route53](https://us-east-1.console.aws.amazon.com/route53/v2/hostedzones#) handles URL routing
- [Test cluster](https://console-openshift-console.test.services.jtech.se/dashboards), with environments Dev, U1, I1, T1 and T2
- [Prod cluster](https://console-openshift-console.prod.services.jtech.se/dashboards), with PROD environment
- Datomic is the default Taxonomy API database that is run on instances in EC2. There are two
  [Test](https://eu-central-1.console.aws.amazon.com/ec2/v2/home?region=eu-central-1#Instances:v=3;instanceState=running;search=:tax-test) and two
  [Prod](https://eu-central-1.console.aws.amazon.com/ec2/v2/home?region=eu-central-1#Instances:v=3;instanceState=running;search=:tax-prod) instances,
  plus the bastion instances.
  The setup has been done by following these [instructions](https://docs.datomic.com/cloud/getting-started/getting-started.html).
- [Datahike file backend in EFS](https://eu-central-1.console.aws.amazon.com/efs/home?region=eu-central-1#/file-systems/fs-0502f1227d4c0b00d),
  an alternative backend for the Taxonomy API database

## Taxonomy

### Systems

The Taxonomy consists of a couple of running services:
- The [Editor](http://taxonomy-editor-prod-taxonomy-frontend-gitops.prod.services.jtech.se/),
  a front-end application for editing the taxonomy,
  [doc](https://gitlab.com/arbetsformedlingen/taxonomy-dev/frontend/jobtech-taxonomy-editor)
- The [Atlas](https://atlas.jobtechdev.se),
  a front-end application for browsing the taxonomy,
  [doc](https://gitlab.com/arbetsformedlingen/taxonomy-dev/frontend/jobtech-taxonomy-atlas)
- Varnish Cache, a caching service infront of the Taxonomy API,
  [doc](https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/openshift-varnish)
- [Taxonomy API](https://taxonomy.api.jobtechdev.se), the backend service handling the taxonomy,
  [doc](https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/jobtech-taxonomy-api)
- Datomic/Datahike, the database where the taxonomy is stored

## Natural language processing (NLP)

### Systems

- The [Annotation Editor](http://annotation-editor-dev-taxonomy-frontend-gitops.test.services.jtech.se/), a tool used to annotate documents,
  [doc](https://gitlab.com/arbetsformedlingen/taxonomy-dev/frontend/jobtech-annotation-editor)
- The Mentor API (The Annotation Editor backend), [doc](https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/nlp/mentor-api)

## Monitoring

There are a few systems set up to monitor the the behavior of the other systems.

- [Bröl](https://mattermost.jobtechdev.se/batfish/channels/brol) a Mattermost channel that logs error messages,
  [doc](https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/broel)
- Statping; [Onprem](https://statping.arbetsformedlingen.se/) and [external](https://batfish-statping.jobtechdev.se/) 
- The [Taxonomy Metrics in ELK](https://elk.jobtechdev.se/app/dashboards#/view/3df7ac10-73b1-11ec-9dd2-09dcfbbcdef6)
