## This is list of all repos we have

| Name                                    | Purpose                                                                  | Deploy           | Archive  | 
|-----------------------------------------|--------------------------------------------------------------------------|------------------|----------|          
| [autocomplete-demo][1]                  | Download,build and install jobtech-taxonomy-autocomplete-occupation-name | No               | No       |
| [autocomplete-demo-pages][2]            |                                                                          | No               | No       |
| [hackathon202403][3]                    |                                                                          | No               | No       |
| [autocomplete-occupation-name][4]       | Javascript package for the autocompleting skills from taxonomy version 18| No               | No       |
| [opensearch-scraper][5]                 | Download logs from OpenSearch and save to a sqlite database              | No               | No       |
| [Taxonomy Specification][6]             |                                                                          | No               | No       |
| [taxonomy-data][7]                      |                                                                          | No               | No       |
| [jobtech-atlas-landingpage][8]          | Updates web content on the Jobtech Atlas landing page                    | Testing and Prod | No       |
| [jobtech-atlas-landingpage-infra][9]    | Infra for Jobtech Atlas landing page                                     | Testing and Prod | No       |
| [jobtech-taxonomy-atlas][10]            | Site for users to browse the JobTech Taxonomy Database                   | Testing and Prod | No       |
| [jobtech-taxonomy-atlas-infra][11]      | Infra for Jobtech Atlas                                                  | Testing and Prod | No       |
| [jobtech-taxonomy-editor][12]           | For edit Taxonomy                                                        | Testing and Prod | No       |
| [jobtech-taxonomy-editor-infra][13]     | Infra for Taxonomy Editor                                                | Testing and Prod | No       |
| [Taxonomy Atlas][14]                    | Rebuild Atlas using Digi - Designsystem                                  | No               | No       |
| [JobTech Taxonomy][15]                  | Describes repositories and activities related to the JobTech Taxonomy    | No               | No       |
| [Documentation][16]                     | Collection of documents related to the work of the taxonomy team         | No               | No       |
| [incident-postmortem-reports][17]       | Document what happened and analyse the causes                            | No               | No       |
| [mdbook-builder][18]                    | Simple builder for mdBook and some of its plugins                        | No               | No       |
| [Secret Documents][19]                  | Secret documents for taxonomy team                                       | No               | No       |
| [test-ipf][20]                          | For testing IPF                                                          | No               | No       |
| [datahike-benchmark][21]                | Compare query times for different versions of Datahike and Datomic       | No               | No       |
| [taxonomy-scripts][22]                  | Scripts for short-term and small tasks related to the taxonomy           | No               | No       |
| [Text2ssyk Experimental][23]            | Multiple experiments of classification job advertisement texts to SSYK4  |                  | No       |
| [varnalyze][24]                         | Analyzing logs from opensearch and simulating Varnish                    | No               | No       |
| [wikipoc-data][25]                      |                                                                          |                  | No       |
| [Datahike][26]                          | Documant about Datahike                                                  | No               | No       |
| [ring-swagger-ui][27]                   | Add the dependency and you have full Swagger-UI ready                    | No               | No       |
| [wanderung][28]                         | Migration tool for Datahike from and to other databases                  | No               | No       |
| [mentor-api-develop][29]                | Annotations repo for develop                                             |                  | No       |
| [mentor-api-prod][30]                   | Annotations repo for prod                                                |                  | No       |
| [mentor-api-test][31]                   | Annotations repo for test                                                |                  | No       |
| [kollektivavtal-annotation][32]         |                                                                          |                  | No       |
| [annotated-document-stats][33]          | Analyze the annotations that have been made and find any inaccuracies    | No               | No       |
| [Bambi][34]                             | BERT for labor market concept recognition.                               | No               | No       |
| [concept-distance-evaluation][35]       | Evaluating the quality of concept recommendations                        | No               | No       |
| [conconopt][36]                         | Software for optimizing connections between concepts from different SKOS | No               | No       |
| [conrec-utils][37]                      | Python3 utilities for concept recognition                                | No               | No       |
| [annotated-with-kollektivavtal][38]     |                                                                          | No               | No       |
| [green-skills-detector][39]             |                                                                          | No               | No       |
| [jae-concept-labels][40]                | Tying concepts of structures, the taxonomy and the synonym dictionary    | No               | No       |
| [jae-conrec][41]                        | Software that uses Jobad Enrichments under the hood to find terms in text| No               | No       |
| [Job ad triplet generator][42]          | Creates triples of historical job postings through the dataset           | No               | No       |
| [Job ads more occupation-name][43]      |                                                                          | No               | No       |
| [jobads-for-sbert][44]                  | Generate training data for Sentence-BERT                                 | No               | No       |
| [jobtech-nlp-evaluation][45]            | Evaluating the effectiveness of NLP solutions developed at Jobtech       | No               | No       |
| [jobtech-nlp-word-embeddings][46]       | Build vector representations of the Taxonomy                             | No               | No       |
| [Mentor Api][47]                        | Mentoring the creation of CVs and Job Postings                           | Testing and Prod | No       |
| [mentor-api-infra][48]                  | Infra for Mentor API                                                     | Testing and Prod | No       |
| [semantic concept search][49]           | Semantic concept search using Sentence-BERT model                        | Testing and Prod | No      |
| [semantic-concept-search-infra][50]     | Infra for semantic-concept-search                                        | Testing and Prod | No       |
| [Taxonomy vec][51]                      | Helper library for distributing large vector model files via nexus       | No               | No       |
| [taxonomy-data-collection-server][52]   | A prototype server that collects data for machine-learning algorithms    | No               | No       |
| [text2ssyk-bert][53]                    | Classifier that classifies job ads to SSYK4 occupation classification    |                  | No       |
| [word2vec][54]                          |                                                                          | No               | No       |
| [jobtech-cljutils][55]                  | A library of utilities to use in Clojure at JobTech                      | No               | No       |
| [jobtech-taxonomy-api][56]              | REST API for the JobTech Taxonomy Database                               | Testing and Prod | No       |
| [jobtech-taxonomy-api-backup-tool][57]  | Contains rules to download and upload databases                          | No               | No       |
| [jobtech-taxonomy-api-infra][58]        | Infra for jobtech-taxonomy-api                                           | Testing and Prod | No       |
| [jobtech-taxonomy-api-libraries][59]    | Library versions of the Taxonomy to common library repositories          | No               | No       |
| [jobtech-taxonomy-backup-files][60]     |                                                                          | No               | No       |
| [jobtech-taxonomy-database][61]         | Taxonomy database management utilities for JobTech                       | No               | No       |
| [JobtechTaxonomyDotnet6][62]            | Build a package with C#.Net                                              | No               | No       |
| [soap-service-java][63]                 | Legacy SOAP Service of the Taxonomy                                      | Testing and Prod | No       |
| [soap-service-java-infra][64]           | Infra for Legacy SOAP Service                                            | Testing and Prod | No       |
| [openshift-varnish][65]                 | Cache requests to the Taxonomy API                                       | Testing and Prod | No       |
| [openshift-varnish-infra][66]           | Infra for openshift-varnish                                              | Testing and Prod | No       |
| [Rapid Infra Development][67]           |                                                                          | No               | No       |
| [tax-analytics][68]                     | Analyze the logs of jobtech-taxonomy-api and run performance benchmarks  | No               | No       |
| [taxonomy-backup-job][69]               | Backup job                                                               | Testing(Develop) | No       |
| [taxonomy-backup-job-infra][70]         | Infra taxonomy-backup-job                                                | Testing(Develop) | No       |
| [Clojure Gitlab Analysis][71]           |                                                                          | No               | Yes      |
| [Datomic cloud REPL proxy][72]          | Datomic cloud REPL proxy                                                 | No               | Yes      |
| [Taxonomy Public Build And Deploy][73]  |                                                                          | No               | Yes      |
| [Nlp Mock Gui][74]                      | Elm UI prototype to test autocomplete                                    | No               | Yes      |
| [Reitit][75]                            |                                                                          | No               | Yes      |
| [af-branch][76]                         |                                                                          | No               | Yes      |
| [broel][77]                             | A ELK-stack log alert                                                    | No               | Yes      |
| [ci-tools][78]                          | Tools to use with the Gitlab CI/CD system used to deploy the Taxonomy    | No               | Yes      |
| [data][79]                              | Data for train ML model                                                  | No               | Yes      |
| [datalog-tool][80]                      | Copy stuff using nippy                                                   | No               | Yes      |
| [dd-minimal-lein][81]                   | A minimal deep diamond project for experimentation                       | No               | Yes      |
| [doom2][82]                             |                                                                          | No               | Yes      |
| [enriched_keywords][83]                 | List the most relevant enriched keywords to be added in the taxonomy     | No               | Yes      |
| [esco skills to taxonomy skills][84]    | Mapping ESCO Skills to Taxonomy Skills                                   | No               | Yes      |
| [general-planning][85]                  | Empty                                                                    | No               | Yes      |
| [gitops][86]                            | Declarative build and deploy setup for the Jobtech Taxonomy Fronted      | No               | Yes      |
| [green skills][87]                      | Relations between green skills and other concepts in Taxonomy            | No               | Yes      |
| [job-ad-conll-parser][88]               |                                                                          | No               | Yes      |
| [jobtech-annotation-editor][89]         |                                                                          | No               | Yes      |
| [jobtech-annotation-editor-infra][90]   | Infra for jobtech-annotation-editor                                      | No               | Yes      |
| [jobtech-api-test][91]                  |                                                                          | No               | Yes      |
| [jobtech-api-test-client-infra][92]     | Infra for jobtech-api-test                                               | No               | Yes      |
| [jobtech-collaborative-stack][93]       |                                                                          | No               | Yes      |
| [taxonomy-api-client-examples][94]      | Provide some examples of how to use the Jobtech Taxonomi API             | No               | Yes      |
| [jobtech-taxonomy-api-gitops][95]       | Declarative build and deploy setup for the Jobtech Taxonomy API          | No               | Yes      |
| [jobtech-taxonomy-api-test][96]         | Tests for the API server in Taxonomy                                     | No               | Yes      |
| [jobtech-taxonomy-backup][97]           | The code is obsolete and has been replaced by in taxonomy-api-gitops     | No               | Yes      |
| [jobtech-taxonomy-common][98]           |                                                                          | No               | Yes      |
| [jobtech-taxonomy-data][99]             | Copies of the taxonomy production database stored in the nippy format    | No               | Yes      |
| [jobtech-taxonomy-example][100]         | Taxonomy example                                                         | No               | Yes      |
| [jobtech-taxonomy-example-infra][101]   | Infra for jobtech-taxonomy-example                                       | No               | Yes      |
| [jobtech-taxonomy-highscore][102]       | Table that lists the frequency how often employer requests certain words | No               | Yes      |
| [jobtech-taxonomy-highscore-infra][103] | Infra for jobtech-taxonomy-highscore                                     | No               | Yes      |
| [taxonomy-legacy-postalcodes][104]      |                                                                          | No               | Yes      |
| [jobtech-taxonomy-nginx][105]           |                                                                          |No                | Yes      |
| [taxonomy-public-deploy-tool][106]      | Declarative build and deploy setup for the Jobtech Taxonomy API          | No               | Yes      |
| [jobtechdev-forum-regler][107]          |                                                                          | No               | Yes      |
| [jobtechdev-site-cms][108]              | The old one jobtechdev-se                                                | No               | Yes      |
| [jobtechdev-site-theme][109]            | The old one jobtechdev-se                                                | No               | Yes      |
| [legacy-taxonomy][110]                  |                                                                          | No               | Yes      |
| [minimal-dd][111]                       | Minimal setup for deep diamond                                           | No               | Yes      |
| [nlp-mock-gui-elm][112]                 |                                                                          | No               | Yes      |
| [nominatim][113]                        |                                                                          | No               | Yes      |
| [nominatim-infra][114]                  | Infra for nominatim                                                      | No               | Yes      |
| [qd-education-data-fetcher][115]        |                                                                          | No               | Yes      |
| [searchtrends-explorer][116]            |                                                                          | No               | Yes      |
| [simple-tokenizer][117]                 |                                                                          | No               | Yes      |
| [skills for AUB][118]                   | Contains candidate skills for AUB                                        | No               | Yes      |
| [sun concepts][119]                     | Contains relations between sun concepts and other concepts in Taxonomy   | No               | Yes      |
| [synonym dic to taxonomy skills][120]   | Most common connected concept between synonym dic and taxonomy skills    | No               | Yes      |
| [taxonomy-autocomplete][121]            | Creates a lucene index for all concepts in the Jobtech Taxonomy          | No               | Yes      |
| [autocomplete-lucene-indexes][122]      |                                                                          | No               | Yes      |
| [taxonomy-database-dist][123]           | Holds taxonomy data in the nippy format                                  | No               | Yes      |
| [taxonomy-mapping-tools][124]           | Establishing mappings from a set of concepts/words to another            | No               | Yes      |
| [taxonomy-postgres][125]                |                                                                          | No               | Yes      |
| [taxonomy-soap-util][126]               |                                                                          | No               | Yes      |
| [taxonomy-static-builder][127]          |                                                                          | No               | Yes      |
| [taxonomy-transformers-clj-py-ner][128] |                                                                          | No               | Yes      |
| [taxonomy-version-db-tool][129]         | Used to deploy a new Taxonomy version to prod                            | No               | Yes      |
| [test-clojure-python-bert-api][130]     | Test system for learning how to launch things in OpenShift               | No               | Yes      |
| [update-ipf][131]                       | Update IPF                                                               | No               | Yes      |
| [wikipoc][132]                          |                                                                          | No               | Yes      |
| [wikipoc-infra][133]                    | Infra for wikipoc                                                        | No               | Yes      |
| [wikipoc-nginx][134]                    | Nginx for wikipoc                                                        | No               | Yes      |




[1]: https://gitlab.com/arbetsformedlingen/taxonomy-dev/experimental/autocomplete-demo
[2]: https://gitlab.com/arbetsformedlingen/taxonomy-dev/experimental/autocomplete-demo-pages
[3]: https://gitlab.com/arbetsformedlingen/taxonomy-dev/experimental/hackathon202403
[4]: https://gitlab.com/arbetsformedlingen/taxonomy-dev/experimental/jobtech-taxonomy-autocomplete-occupation-name
[5]: https://gitlab.com/arbetsformedlingen/taxonomy-dev/experimental/opensearch-scraper
[6]: https://gitlab.com/arbetsformedlingen/taxonomy-dev/experimental/taxonomy-specification#
[7]: https://gitlab.com/arbetsformedlingen/taxonomy-dev/experimental/taxonomy-data
[8]: https://gitlab.com/arbetsformedlingen/taxonomy-dev/frontend/jobtech-atlas-landingpage
[9]: https://gitlab.com/arbetsformedlingen/taxonomy-dev/frontend/jobtech-atlas-landingpage-infra
[10]: https://gitlab.com/arbetsformedlingen/taxonomy-dev/frontend/jobtech-taxonomy-atlas
[11]: https://gitlab.com/arbetsformedlingen/taxonomy-dev/frontend/jobtech-taxonomy-atlas-infra
[12]: https://gitlab.com/arbetsformedlingen/taxonomy-dev/frontend/jobtech-taxonomy-editor
[13]: https://gitlab.com/arbetsformedlingen/taxonomy-dev/frontend/jobtech-taxonomy-editor-infra
[14]: https://gitlab.com/arbetsformedlingen/taxonomy-dev/frontend/taxonomy-atlas
[15]: https://gitlab.com/arbetsformedlingen/taxonomy-dev/projects/jobtech-taxonomy
[16]: https://gitlab.com/arbetsformedlingen/taxonomy-dev/documentation
[17]: https://gitlab.com/arbetsformedlingen/taxonomy-dev/incident-postmortem-reports
[18]: https://gitlab.com/arbetsformedlingen/taxonomy-dev/mdbook-builder
[19]: https://gitlab.com/arbetsformedlingen/taxonomy-dev/secret-documents
[20]: https://gitlab.com/arbetsformedlingen/taxonomy-dev/test-ipf
[21]: https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/experimental/datahike-benchmark
[22]: https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/experimental/taxonomy-scripts
[23]: https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/experimental/text2ssyk-experimental
[24]: https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/experimental/varnalyze
[25]: https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/experimental/wikipoc-data
[26]: https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/forks/datahike
[27]: https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/forks/ring-swagger-ui
[28]: https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/forks/wanderung
[29]: https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/nlp/annotations/mentor-api-develop
[30]: https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/nlp/annotations/mentor-api-prod
[31]: https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/nlp/annotations/mentor-api-test
[32]: https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/nlp/ads-with-missing-kollektivavtal-annotation
[33]: https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/nlp/annotated-document-stats
[34]: https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/nlp/bambi
[35]: https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/nlp/concept-distance-evaluation
[36]: https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/nlp/conconopt
[37]: https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/nlp/conrec-utils
[38]: https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/nlp/files-annotated-with-kollektivavtal
[39]: https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/nlp/green-skills-detector
[40]: https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-concept-labels
[41]: https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/nlp/jae-conrec
[42]: https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/nlp/job-ad-triplet-generator
[43]: https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/nlp/job-ads-with-more-than-one-occupation-name
[44]: https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/nlp/jobads-for-sbert
[45]: https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/nlp/jobtech-nlp-evaluation
[46]: https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/nlp/jobtech-nlp-word-embeddings
[47]: https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/nlp/mentor-api
[48]: https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/nlp/mentor-api-infra
[49]: https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/nlp/semantic-concept-search
[50]: https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/nlp/semantic-concept-search-infra
[51]: https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/nlp/taxonomy-vec
[52]: https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/nlp/taxonomy-data-collection-server
[53]: https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/nlp/text2ssyk-bert
[54]: https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/nlp/word2vec
[55]: https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/jobtech-cljutils
[56]: https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/jobtech-taxonomy-api
[57]: https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/jobtech-taxonomy-api-backup-tool
[58]: https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/jobtech-taxonomy-api-infra
[59]: https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/jobtech-taxonomy-api-libraries
[60]: https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/jobtech-taxonomy-backup-files
[61]: https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/jobtech-taxonomy-database
[62]: https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/jobtechtaxonomydotnet6
[63]: https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/legacy-taxonomy-soap-service-java
[64]: https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/legacy-taxonomy-soap-service-java-infra
[65]: https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/openshift-varnish
[66]: https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/openshift-varnish-infra
[67]: https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/rapid-infra-development
[68]: https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/tax-analytics
[69]: https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/taxonomy-backup-job
[70]: https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/taxonomy-backup-job-infra
[71]: https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/clojure-gitlab-analysis
[72]: https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/datomic-cloud-repl-proxy
[73]: https://gitlab.com/arbetsformedlingen/taxonomy-dev/frontend/jobtech-taxonomy-public-build-and-deploy
[74]: https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/nlp/nlp-mock-gui
[75]: https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/forks/reitit
[76]: https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/experimental/af-branch
[77]: https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/broel
[78]: https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/ci-tools
[79]: https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/nlp/data
[80]: https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/experimental/datalog-tool
[81]: https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/nlp/deep-diamond/dd-minimal-lein
[82]: https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/doom2
[83]: https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/nlp/enriched_keywords
[84]: https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/nlp/esco-skills-to-taxonomy-skills
[85]: https://gitlab.com/arbetsformedlingen/taxonomy-dev/general-planning
[86]: https://gitlab.com/arbetsformedlingen/taxonomy-dev/frontend/gitops
[87]: https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/nlp/green-skills
[88]: https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/nlp/job-ad-conll-parser
[89]: https://gitlab.com/arbetsformedlingen/taxonomy-dev/frontend/jobtech-annotation-editor
[90]: https://gitlab.com/arbetsformedlingen/taxonomy-dev/frontend/jobtech-annotation-editor-infra
[91]: https://gitlab.com/arbetsformedlingen/taxonomy-dev/frontend/jobtech-api-test-client
[92]: https://gitlab.com/arbetsformedlingen/taxonomy-dev/frontend/jobtech-api-test-client-infra
[93]: https://gitlab.com/arbetsformedlingen/taxonomy-dev/jobtech-collaborative-stack
[94]: https://gitlab.com/arbetsformedlingen/taxonomy-dev/frontend/jobtech-taxonomy-api-client-examples
[95]: https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/jobtech-taxonomy-api-gitops
[96]: https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/jobtech-taxonomy-api-test
[97]: https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/jobtech-taxonomy-backup
[98]: https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/jobtech-taxonomy-common
[99]: https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/jobtech-taxonomy-data
[100]: https://gitlab.com/arbetsformedlingen/taxonomy-dev/frontend/jobtech-taxonomy-example
[101]: https://gitlab.com/arbetsformedlingen/taxonomy-dev/frontend/jobtech-taxonomy-example-infra
[102]: https://gitlab.com/arbetsformedlingen/taxonomy-dev/frontend/jobtech-taxonomy-highscore
[103]: https://gitlab.com/arbetsformedlingen/taxonomy-dev/frontend/jobtech-taxonomy-highscore-infra
[104]: https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/jobtech-taxonomy-legacy-postalcodes
[105]: https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/jobtech-taxonomy-nginx
[106]: https://gitlab.com/arbetsformedlingen/taxonomy-dev/frontend/jobtech-taxonomy-public-deploy-tool
[107]: https://gitlab.com/arbetsformedlingen/taxonomy-dev/jobtechdev-forum-regler
[108]: https://gitlab.com/arbetsformedlingen/taxonomy-dev/frontend/jobtechdev-site-cms
[109]: https://gitlab.com/arbetsformedlingen/taxonomy-dev/frontend/jobtechdev-site-theme
[110]: https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/legacy-taxonomy
[111]: https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/nlp/minimal-dd
[112]: https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/nlp/nlp-mock-gui-elm
[113]: https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/nominatim
[114]: https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/nominatim-infra
[115]: https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/nlp/qd-education-data-fetcher
[116]: https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/jobsearch-trends-log-manager
[117]: https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/nlp/simple-tokenizer
[118]: https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/nlp/skills-for-aub
[119]: https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/nlp/sun-concepts
[120]: https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/nlp/synonym-dictionary-to-taxonomy-skills
[121]: https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/taxonomy-autocomplete
[122]: https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/taxonomy-autocomplete-lucene-indexes
[123]: https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/taxonomy-database-dist
[124]: https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/nlp/taxonomy-mapping-tools
[125]: https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/experimental/taxonomy-postgres
[126]: https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/taxonomy-soap-util
[127]: https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/taxonomy-static-builder
[128]: https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/nlp/taxonomy-transformers-clj-py-ner
[129]: https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/taxonomy-version-db-tool
[130]: https://gitlab.com/arbetsformedlingen/taxonomy-dev/demo/test-clojure-python-bert-api
[131]: https://gitlab.com/arbetsformedlingen/taxonomy-dev/update-ipf
[132]: https://gitlab.com/arbetsformedlingen/taxonomy-dev/frontend/wikipoc
[133]: https://gitlab.com/arbetsformedlingen/taxonomy-dev/frontend/wikipoc-infra
[134]: https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/experimental/wikipoc-nginx









