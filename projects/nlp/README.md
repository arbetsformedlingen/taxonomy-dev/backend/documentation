# Natural Language Processing

## Problems that we are working on

In this section, we explain the problems that we are working on.

### Semantic Annotation of Ads

By **semantic annotation** [1] we refer to the process of taking a natural text and find segments in that text that can be associated with concepts in the taxonomy. More precisely, the *input* of a semantic annotation algorithm is a piece of natural text, and the *output* is a list of concept ids and their location within the text. This process can be done by a human or a machine or a combination of human and machine. We are studying a ways of automating this task by a machine to reduce human effort.

Being able to associate segments of written text can be useful in many ways:

* Definition of a particular concept detected in the text can be found
* Documents can be organized based on taxonomy concepts
* Documents can be searched for based on taxonomy concepts

We also consider a relaxed variant of semantic annotation that we will refer to **unstructured semantic annotation**: this relaxed variant differs from the previous variant in that the algorithm does not output the location of a detected concept in text. Just like regular semantic annotation it takes an input text, but the output is simply *a set of concept ids* found in the text and not their locations.

### Concept Recommendations

Concept recommendations can be compared to recommendations that an online shopper may receive while shopping online and filling a virtual shopping cart with products. The idea is that, given a set of at least one concept, we want to come up with one or more related concepts. For instance, a job seeker who is publishing his CV may associate a set of taxonomy concepts with that CV. A recommendation engine can suggest more concepts to be associated with the CV, thereby helping the job seeker with the task of filling out his CV.

Another application is a semantic search engine for job ads: The user of the search engine would choose a few concepts to find ads that contain those concepts. A recommendation engine can suggest additional concepts to include in the query in order to get more accurate results.

## Approaches

Here we list noteworthy approaches for addressing the above problems.

### Approaches for Semantic Annotation of Ads

#### Word-based matching

Go through the text, word by word, and whenever the word matches the label of a concept, add that match as an annotation.

#### Deriving concepts from keywords (detected synonym)

Apply JobAd Enrichments to a text to detect keywords that also occur in the taxonomy. Then link the keywords to concepts of relevant types, such as `occupation-name` and `skill`.

#### Linear combination of SBERT vectors

The idea is to use a pre-trained SBERT model to perform **unstructured semantic annotation** of an add. The steps are as follows:

1. Compute an SBERT vector `Y` for the ad to be processed.
2. Given precomputed SBERT vectors `X_i` for all concepts `i`, minimize w.r.t. `L = [..., lambda_i, ...]` the objective `f(L, tao) = |tao*mu + sum_i(lambda_i*X_i) - Y|^2 + alpha*|L|`, where `L` is the vector of `lambda_i`, `tao` is a scaling factor and `mu` is the mean embedding computed over a training set of ads, and `alpha` is a regularization constant. Minimizing this objective function means finding a linear combination of concepts that represent the embedding of the ad. The regularization term encourages sparsity, thereby limiting the number of concepts that are detected.
3. Form the set of detected concepts as all concepts `i` for which the corresponding `lambda_i` is positive and significantly greater than 0.

#### Semantic annotation based on token classification (SABOTOC)

The idea would be to formulate the semantic annotation problem as a token classification problem, where we use BERT for token classification. The algorithm would consider one taxonomy concept at a time and classify each token of the input text document as being part of that taxonomy concept or not. The input to BERT would be on the form

```
[CLS] {label(s) of the concept} [SEP] {input text} [SEP]
```
and the output would be logit values onto which a binary softmax is performed to obtain discrete values of whether a token is the concept that we are looking for, or not. If this token classification is performed for each of the concepts, then we can for each token choose the concept with the highest logit value that is above a certain threshold.

The BERT model that performs the above token classification can be trained on an abundance of historical job ads that have been enriched with synonyms. The input to BERT would then be on the form

```
[CLS] {subset of synonyms within a synonym group} [SEP] {job ad} [SEP]
```
and the expected output would be based on the output of JobAd enrichments applied to that job ad. Along with this, some training data can also be obtained from texts that have been manually annotated with concepts, but that data is currently much less abundant.

Task breakdown:
1. Generate training data from historical ads and JobAd Enrichments
2. Train BERT for token classification on training data
3. Evaluate the BERT on both other job ads enriched with JobAd Enrichments *and* a dataset of documents that have been manually annotated with concepts.

### Approaches for Concept Recommendations

#### Recommendations based on SBERT

Map the labels of all concepts to vectors using, for example, SBERT. Given a concept, we can generate recommendations for that concept by taking the concepts whose vectors result in the greatest cosine distance to the query concept.

As an extension to the above approach, a linear transformation can be applied to the vector first. So instead of measuring the cosine between vectors `X` and `Y`, we measure the cosine between vectors `AX + B` and `AY + B`. The matrix `A` and the vector `B` could be computed based on for example PCA [2]. Or they can be optimized given some training data and a suitable objective function.

#### Training data for recommendations

Because the purpose of the recommendations are to fill in what is missing, one way of generating training data for recommendations would be to start from text documents, e.g. job ads or education ads. For each ad, we assume there is an effective algorithm to detect a set of concepts. From this set of concepts, we can randomly omit one concept and the task for a machine learning algorithm would be to predict what concept is missing.

## Evaluation


### Evaluation of semantic annotations

Manually annotated documents that can be used for evaluating semantic annotation algorithms are found in [this git repository](https://gitlab.com/taxonomy-annotations/annotations) and produced using a special [editor for annotating documents](http://annotation-editor-dev-taxonomy-frontend-gitops.test.services.jtech.se/).

The actual evaluating is done by computing the *precision*, *recall* and *F1* scores per concept and averaging them. These metrics are computed when generating a report, see [explanation for reproducing results](https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/nlp/nlp-education-analyzer#results).

### Evaluation of recommendations

A recommendation algorithm can be evaluated using **triplets** if, given an **anchor** concept, it can answer the question whether a concept A is closer to the anchor than the concept B. For this purpose, there is [a dataset](https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/nlp/concept-distance-evaluation) of such triplets consisting of **anchor concept**, **positive concept** and **negative concept**. To evaluate an algorithm, it has to answer the following question: "Is it true that the positive concept is closer to the anchor than the negative concept?". Then the number of triplets for which the algorithm produced the correct answer is counted.

## More Information

Here, we collect documents about natural language processing related to the taxonomy.

* [Literature](literature.md): List of relevant scientific literature to our problem.

## References

[1] Jovanović, Jelena, and Ebrahim Bagheri. "Semantic annotation in biomedicine: the current landscape." Journal of biomedical semantics 8.1 (2017): 1-18.
[2] Huang, Junjie, et al. "Whiteningbert: An easy unsupervised sentence embedding approach." arXiv preprint arXiv:2104.01767 (2021).
