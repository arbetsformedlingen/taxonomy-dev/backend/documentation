# Taxonomy as Software Libraries

The purpose is to make the taxonomy easier to consume by other software developers by delivering it in the form of [software libraries][100].

## Requirements

* Given a concept id, it should be possible to check whether that concept exists or not.
* Given a concept id, it should be possible to check whether that concept is a skill.

## Tasks

Here is a task breakdown.

### Build Python library [ONGOING]

A first attempt at generating a Python library can be found [here][101]. It downloads the entire taxonomy from [data.jobtechdev.se][102].

### Export the taxonomy to Java [NOT STARTED]

### Export the taxonomy to C# [NOT STARTED]

## Links

| Name                                                          | Description                                                      |
|---------------------------------------------------------------|------------------------------------------------------------------|
| [`jobtech-taxonomy-api-libraries`][100]                       | Main repository for the libraries and the code to generate them. |
| [`jobtech-taxonomy-api-libraries Python implementation`][101] | The taxonomy in the form of a Python library.                    |
| [data.jobtechdev.se][102]                                     | The taxonomy  in the form of downloadable files.                 |


[100]: https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/jobtech-taxonomy-api-libraries
[101]: https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/jobtech-taxonomy-api-libraries/-/tree/trunk/jobtech-taxonomy-library-python
[102]: https://data.jobtechdev.se/taxonomy/version/
